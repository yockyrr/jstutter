---
title: "How–to: Transform XML in Browser (Answer: Don’t)"
permalink: false
date: 2020-08-16 21:12:00 +1
---

In my first post, I mentioned that my next milestone was to be able to transform
XML using XSLT:

> In–browser soon, for all browsers that support `XSLTProcessor` (that is, not
> IE).

The harsh reality is that although XSLT is “supported” by browsers, you will be
tricked at every turn by a technology that, although basically functional, has
not been updated with any new API for over a decade. Your lovely, performant
stylesheet that worked fine using libXML’s `xsltproc`[^1] will fail, seemingly for
no reason, in browsers and, as mentioned, will never work in IE.

Good luck! Here are the particular hurdles that I came up against, and how I
overcame, hacked, or just plain gave up on them. My goal was to be able to
transform the data that will be input into my app’s database into a “useful”
format, something that can be parsed by humans as well as computer and then
simply transformed into a viewable edition and other useful formats such as
MEI.[^2] However, I was deceived along the way by the call of “the right way to
do things” and stepped upon the path of XML, which lead me directly to hell.
This is part of the story of how I am still trying to get out!

At the end of my last post, I had some lovely (and not so lovely) XML transform
(XSLT) stylesheets, as well as an API endpoint that could spit out some XML to
be transformed by these XSLTs. One small stylesheet can scrape the XML for
syllables, thus extracting the text of a setting (`candrToSettingText.xsl`).[^3]
Another huge and ugly stylesheet (`candrSettingToMei.xsl`)[^4] can transform
this XML into an odd dialect of MEI, and one final stylesheet
(`MeiToABC.xsl`)[^5] can convert this MEI oddity into ABC, a text–based music
typesetting language. My API can convert database tables into XML, then by
chaining XML transforms together I can convert from XML to ABC, then ABC to
printable editions in SVG or PDF etc. My issues arise with that second step:
chaining XML transforms.

# The first dead end: XSLTProcessor

I had initially shied away from performing XML transforms on the server as I
knew that `candrSettingToMei.xsl` in particular can take several seconds to run
on large settings due to its matrix transformations, and I did not wish to tie
up my backend with needless XML transforms every time a page is requested. I
believe an evil man once said, ‘Browser CPU cycles are free’, and he was
technically and unfortunately correct. If I could offload XML transforms and
their delays to the client rather than tying up my server, then I would be able
to dedicate more server time to generating XML rather than transforming it.

`XSLTProcessor` has been around in most browsers since the very beginning, and
this makes sense as there were some dark days of XHTML and stylesheets other
than CSS.[^6] It turns out that IE can in fact do XML transforms but it is
hidden within… an ActiveX control *shudders*. Luckily, there is a simple wrapper
around these conflicting standards in `murrayju/xslt`,[^7] which exposes a
single function for doing XML transforms. Ideal! After importing the package
into my project, I created some classes to consume XSLT jobs and despatch them
to a Web Worker thread, as I do not want to halt the main thread to do XML
transforms, but instead keep them running in the background and use the main
thread to provide updates to the user.

This provided the first big sticking point: Web Workers cannot use
`XSLTProcessor` or similar.

# What? … Why?

I’ll be the first to admit: this makes zero sense. Obviously, Web Workers cannot
manipulate the DOM as they operate outside of the main thread and don’t want to
create race conditions in the DOM. All DOM methods are off–limits to them. For
some bizarre reason, this includes `XSLTProcessor`, perhaps because it is
technically manipulating **a** DOM. Not **the** DOM, but a DOM nonetheless.

A no–go then, but are there vanilla JavaScript libraries to perform the same
function as `XSLTProcessor` without using that API? Yes there are, kind of, but
they are incomplete and lack even basic functionality. Moreover, my stylesheets
use EXSLT for all different kinds of functions,[^8] and no JavaScript–based XSLT
processor shows any sign of an attempt to comply with any part of EXSLT.

So it looked like I would have to use XSLT on the main thread. This might hang
the browser for those few seconds it takes to transform the XML, but I supposed
that the page wasn’t doing anything else particularly, and needs must.

# XInclude

My original XML made liberal use of XInclude, which is a simple way in which
to include other XML files in your XML. All you have to do is to set a link to
another XML file, and provide a fallback (usually an error) in case that link
didn’t work for some reason, like so:

    <xi:include href="/xml/set/7/">
    	<xi:fallback>
            <candr:error>xinclude: /xml/set/7/ not found</candr:error>
        </xi:fallback>
    </xi:include>

`xsltproc` has a great flag: `--xinclude` which, you guessed it, performs
‘XInclude processing on document input’, i.e. before processing. This works
great, but all the requests slow down the processing and increases load to the
server, which ends up having to serve multiple XML files rather than doing one
huge SQL `SELECT *` across the database which, although not ideal, is a fair
sight better. Unfortunately, `XSLTProcessor` has no such flag so before
processing, the XML will have to undergo XInclude processing. MDN has a snippet
to resolve XIncludes using JavaScript and, after porting to CoffeeScript (my
language of choice, I have my reasons) it… didn’t work.[^9] Well, I suppose
that’s what you get for trying a page with code from 2008?

The alternative is to attempt to do the XInclude on the server when requesting
the XML. I first tried PHP’s support for XInclude, but ran into difficulties
straightaway. PHP’s development server is single–threaded only, so cannot handle
another request while another is active. Put another way, the server is
attempting to do XIncludes as it goes, and each XInclude it comes across is a
URL, making another request to the same server, which is already tied up trying
to deal with the XIncludes in the first place! The obvious solution would be to
use a “real” server rather than just a toy development server, but there must be
a better way than spawning hundreds of threads just to generate one XML file?

The answer was to generate a subpath in my routes. Each XML endpoint now has a
subpath called `/xinclude/` which does an effective XInclude, but through PHP
rather than the XInclude API. Each generator function that generates my XML now
has a boolean flag added to its signature indicating whether to generate an
XInclude for an element or to include it directly, and that flag is passed
recursively down through the functions. This is an order of magnitude faster as
we are making a single request to the API rather than hundreds. When doing XSLT,
I‘ll just have to remember to request the `/xinclude/` subpath rather than the
base point.

# Error: There was an error somewhere, idk, stop asking, leave me alone!

Returning once again to `XSLTProcessor`, after busily refactoring my lovely job
controller into a simple procedural function (yes I know I should have worked on
the main thread before putting it into a worker, lesson learned), I finally had
a simple class that loaded XSLTs using AJAX and fed them one–by–one into
`XSLTProcessor`. Time to test it out:

    Uncaught Exception
        columnNumber: 0
        data: null
        filename: "debugger eval code"
        lineNumber: 1
        message: ""
        name: ""
        result: 2153775105
        stack: "@debugger eval code:1:3\n"
        <prototype>: ExceptionPrototype { toString: toString(), name: Getter, message:Getter, …}

What is that, Firefox? This error message appeared as soon as I tried to import
the stylesheet using my perfectly valid XSLT. I have seen some cryptic messages
in the past, but this error decides not to give any indication of the type of
error that has occurred, apart from that number 2153775105. A good hour of
searching yielded only that its hex equivalent (0x80600001) is an error flag in
Firefox called `NS_ERROR_XSLT_PARSE_FAILURE`.[^10] Ah, would have been nice to know
that originally maybe, but it is something at least. It doesn’t like my XSLT. At
least Firefox gave me an error. Chrome just silently returned `undefined` and
left it at that. What the XSLT parse failure was, where, and how it was
incorrect however, I was given no clue, and I really wasn’t inclined to find
out, given that my debugging would just be adding and removing random lines
until it started working. No thank you, I would rather go down a thousand other
routes before trying that.

# Back to the server

After a quick look to see whether some kind soul had ported `xsltproc` to
WebAssembly (they haven’t, and I’m not about to start), I considered my options
on the server again. PHP has `XSLTProcessor` too, and it is a binding around
`xsltproc`. This will definitely work, but I really don’t want to tie up my
server. I considered starting background jobs and running another job server to
send jobs to a queue and send updates on those jobs to the user via AJAX then
pushing the result the same way, but the thought of polling and more JavaScript
made me nauseous as my previous run–in with that nightmare a few years ago left
me reeling. A simple check indicated that yes, `XSLTProcessor` works with my
stylesheet in PHP, and I might as well use that `/xinclude/` endpoint subpath I
made to solve my previous issues, as it is miles faster.

By doing my includes in PHP rather than XSLT, I appear to have inadvertently
solved my own problem a different way as my transforms are much faster with only
the transforming to worry about and not the XInclude-ing. My transforms are now
still slow, but bearable to perform on the server. I could simply set up a
pipeline of processors in PHP to generate the correct output. The final step,
converting to SVG or PDF, will require shelling out to `abcm2ps`, but *should*
be trivial in comparison to what I have just gone through.

[^1]: Daniel Veillard, ‘The xsltproc tool’, *libxml2*, <[http://xmlsoft.org/XSLT/xsltproc2.html](http://xmlsoft.org/XSLT/xsltproc2.html)>
[accessed 16 August 2020]

[^2]: *Music Encoding Initiative*, <[https://music-encoding.org/](https://music-encoding.org/)>

[^3]: Joshua Stutter, ‘xslt/candrToSettingText.xsl’, *CANDR* <[https://gitlab.com/yockyrr/candr/-/blob/master/xslt/candrToSettingText.xsl](https://gitlab.com/yockyrr/candr/-/blob/master/xslt/candrToSettingText.xsl)>
[accessed 16 August 2020]

[^4]: Joshua Stutter, ‘xslt/candrSettingToMei.xsl’, *CANDR* <[https://gitlab.com/yockyrr/candr/-/blob/master/xslt/candrSettingToMei.xsl](https://gitlab.com/yockyrr/candr/-/blob/master/xslt/candrSettingToMei.xsl)>
[accessed 16 August 2020]

[^5]: Joshua Stutter, ‘xslt/MEItoABC.xsl’, *CANDR* <[https://gitlab.com/yockyrr/candr/-/blob/master/xslt/MeiToABC.xsl](https://gitlab.com/yockyrr/candr/-/blob/master/xslt/MeiToABC.xsl)>
[accessed 16 August 2020]

[^6]: ‘XSLTProcessor - Web APIs’, *MDN* <[https://developer.mozilla.org/en-US/docs/Web/API/XSLTProcessor](https://developer.mozilla.org/en-US/docs/Web/API/XSLTProcessor)>
[accessed 16 August 2020]

[^7]: Justin Murray, ‘xslt’, *GitHub* <[https://github.com/murrayju/xslt](https://github.com/murrayju/xslt)>
[accessed 16 August 2020]

[^8]: *EXSLT* <[http://exslt.org/](http://exslt.org/)> [accessed 16 August 2020]

[^9]: ‘XInclude’, *MDN Web Docs Glossary* <[https://developer.mozilla.org/en-US/docs/Glossary/XInclude](https://developer.mozilla.org/en-US/docs/Glossary/XInclude)>
[accessed 16 August 2020]

[^10]: ‘Error codes returned by Mozilla APIs’, *MDN* <[https://developer.mozilla.org/en-US/docs/Mozilla/Errors](https://developer.mozilla.org/en-US/docs/Mozilla/Errors)> [accessed 16 August 2020]
