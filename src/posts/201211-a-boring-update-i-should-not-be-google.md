---
title: "A boring update: I should not be Google"
permalink: false
date: 2020-12-11 20:11:00 +0
---

<figure><img src="images/201211/two-wolves.jpg" alt="Inside you there are two wolves: Do it the RightWay(TM) / Push directly to Production"/></figure>

I have always considered myself further to the right of this image. Nothing
that I have ever created has really had that many consequences, so I prefer to
“move fast and break things” because my projects have nearly always been for my
own benefit. *Onoseconds*[^1] have been entirely my mess to clean up when I
completely break my project. I have been influenced in the way I work by Oz
Nova’s article *You are not Google*,[^2] where he makes the case that it is
extremely tempting to over–engineer solutions when the simplest, most pragmatic
solution will probably do just fine. It is unlikely you really need that
99.99999% uptime.

This has kept me closer to the *laissez–faire* attitude, and I have always been
sceptical of solutions that will feed two birds with one scone, or even solve
all my issues at once! I have found that these solutions will likely bring in
more issues that I didn’t have to contend with before, for example instead of
just changing one thing I will have to go through a whole process to get the
same result (more of that later).

# Moving to the left

My last post was very much an example of that (perhaps too wild) attitude. It
was definitely more of a proof of concept of using such ML techniques to
recognise and locate features on ND polyphony manuscripts. However, the code
that got me there was rushed, hacky and buggy. For example, each model was a
hacked–together Python script. When I created a new model, I foolishly just
copied the script to a new file and changed the bits I needed, rather than
abstracting out the common functionality into other bits of program. I had
perhaps taken too much to heart that quote “premature optimisation is the root
of all evil”.[^3] Regardless, those scripts obviously needed cleaning up at
some point. The git repositories, too, were a mess (now cleaned up into a
different kind of Dockerfile mess).

CANDR as it is, was written in that same vein, but has since been cleaned up.
Not as hacky, but written very simply in good ol’ PHP using a thin framework
and backed by the disk–based SQLite database. However, when I began to think
about how to deploy my machine learning models onto the website, into the cloud
(although I detest that expression), my infrastructure must become more
complex, and I must move closer to the “Right Way” of doing things.

As the machine learning models are fairly CPU intensive and benefit from being
extended by graphics cards (GPUs), it certainly will not do to have them running
on the same service as the website frontend. That way, every time the models are
trained, the website will crash. Not good. I had to deploy the models onto a
different service to the website. However, they also need access to the
database, which right now was locked on the hard drive of the website service.

I began therefore by migrating my simple SQLite database to a more flexible but
complicated PostgreSQL instance on Google Cloud (not the most ethical cloud
provider, but the best of a bad bunch). I went with PostgreSQL as MySQL (the
other option) is really quite difficult to work with when you have lots of
variable–width text fields.

I eventually landed on an infrastructure which works like so:

<figure>

```mermaid
sequenceDiagram
  participant C as CANDR Website
  participant B as Codebase
  participant I as GitLab CI
  participant R as CANDR ML Runner
  participant D as Database
  participant W as CANDR ML Worker
  B->>+I: Change code
  I->>+I: Build website / runner / worker
  I->>+C: Deploy with K8s
  I->>+R: Deploy with K8s
  I->>+W: Deploy with K8s
  C->>+D: Alter database
  Loop: Every 24 hours
    R->>+D: Is database changed?
    D->>+R: Yes
  end
  R->>+I: Trigger training pipeline
  I->>+R: Begin training models
  R->>+W: Provision training service on K8s
  Loop: Every 5 seconds
    I->>+R: Get training status
    R->>+I: Training status
    I->>+I: Display training status in pipeline
  end
  W->>+D: Get the model data
  D->>+W: Model data
  W->>+W: Training models
  W->>+D: Save models
  W->>+D: Get untranscribed data
  D->>+W: Untranscribed data
  W->>+W: Predicting untranscribed data
  W->>+D: Save predictions
  I->>+R: Get training status
  R->>+I: Status done!
  I->>+R: End training
  R->>+W: Unprovision training service
  C->>+D: Get prediction for stave
  D->>+C: Prediction data
```

<figcaption>Sequence diagram of infrastructure</figcaption></figure>

This began as a simple way to train the models, but through various small
necessities has become larger as a move towards the fabled “Right Way” of doing
things. This includes modern technologies such as Continuous Integration /
Deployment (CI), Docker images and Kubernetes (which I have gone from zero
knowledge to a little knowledge on). Simply put:

- CI is a way to make a change to your code and the resulting app or website to
  be automatically built for you.[^4]
- Docker images are pickled versions of services that can be stored as a file,
  uploaded to a registry and then pulled from that registry and deployed at
  will.
- Kubernetes is an architecture that can deploy Docker images with zero downtime
  by maintaining multiple running copies of those services and performing an
  automatic rolling update of those services. In addition, if you have passwords
  or secrets that need to be passed to those services, it can inject those at
  runtime rather than your Docker images being passed around with passwords
  stored inside.

What I initially wished to occur was that a regular job (say every 24 hours)
would spin off and train the machine learning models. However, I soon realised
that if I’m provisioning a large, expensive machine to train the models, I can’t
have it running all the time wasting money here and there. The machine that
serves the website costs only a penny an hour, but a machine powerful enough to
train machine learning models costs a pound an hour. I could not leave that
machine running, but needed to provision it when necessary, and only when there
is new data.

Therefore, I use a level of indirection: a “director” service in *CANDR ML
Runner*,[^5] provisioned on the same machine as the website. The runner checks
the status of the machine learning and is responsible for provisioning the ML
service via Kubernetes (i.e. flicking the big switch to turn the expensive
computer on). This frees up the website, and all other services can check the
runner service to see the status of the ML training. As the runner runs on the
same cheap machine as the website, it does not incur any extra cost and updates
the predictions only when the training and prediction has been completed. This
way, the website can retrieve the most up–to–date predictions from the ML.

Furthermore, the entire build process has been automated using GitLab’s CI. As
soon as a change is made to the master branch of the codebase, the website and
ML tools are rebuilt automatically and redeployed (again via Kubernetes) to
live. This adds another layer of complexity to the diagram above.

It finally works‥ but it is rather reminiscent of this video:

<figure><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/y8OnoxKotPQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><figcaption>A familiar conversation with myself</figcaption></figure>

# Fixing the training code

As mentioned above, the Python scripts were rather hacked together and needed
some abstraction. I have now begun work on cleaning up these scripts into their
constituent functionality. Previously, I had long, repetitive code that did the
same work over and over for each model, but I have now successfully abstracted
work into individual classes to deal with:

- Abstract model as base functionality for all derived models:
  - Staffline line model
  - Staffline ROI model
  - Staffline location model
  - Divisione model
  - Note model
- Each model uses their respective data generator derived from an abstract data
  generator:
  - Staffline line data generator
  - Staffline ROI data generator
  - Staffline location data generator
  - Divisione data generator
  - Note data generator
- Class for drawing stafflines from constituent detected segments
- Image fetching
- Training callbacks for status updating
- Database driver
- ML runner status updating
- Driver app

Rather than a complicated mess of tangled code, the driver app now simply calls
each model one–by–one and calls `train()` followed by `predict()`, and the
abstract classes and data generators take care of the rest. The status is
updated via a simple JSON API on the runner which exposes the status to other
services. This is authenticated by public key encryption challenges for each
status update, so that only the worker is authorised to update the status.

My first conception as to how this should work had to be changed however, as
initially I had the idea to train the models and predict the untranscribed
staves in two discrete stages. However, I soon found out that to train and then
save the models, I would have to save the neural networks that make up the ML
models in some format. There is the h5 format for this purpose, but getting that
format out was tricky, especially as each model when saved in this format came
out as several hundred megabytes in size. I thought that I could simply put that
data in a big column in the database, but it was so slow that it was timing out
the connection, and I really did not want to go down the route of making more
infrastructure just to store the model save files.

I have since altered the process to train and predict in one pass. Each model is
trained on the current data, the untranscribed staves are predicted, and then
that model must be thrown away as it cannot be saved. This is not too much of a
loss however, as I was planning to train the models from scratch each time
anyway.

<figure><video autoplay preload loop muted poster src="images/201211/training-notes.mp4"></video><figcaption>Training models. Little ticking progress bar at the bottom shows some slow progress on training for notes.</figcaption></figure>

# Embarrassing admin

I mentioned also that I was using Google’s Cloud Compute service for much of
this, and I have been toying around with their generous free trial before
spending any real money (however small). Google’s platform needfully sits to the
far left of that first image, doing everything the “Right Way” each time. If
anyone is Google, it’s Google. However, that means that for a small
single–person project, there are a lot of unnecessary hoops to jump through. I
found myself using their interface to grant privileges to myself saying, “Yes, I
will allow myself to use this or that particular function”. In a large company
or project, it is easy to see how this bureaucracy would be useful, with every
employee or team member having their own little walled garden and not
accidentally messing things up by pressing the wrong button. This way of working
was frustrating for me and just served to get in the way as I was going to grant
myself those privileges anyway!

However, I may have got a little too trigger happy granting myself privileges
when I came to the issue of using cloud GPUs to accelerate the ML tasks. Such
computing tasks are better using graphics cards and other accelerators rather
than your computer’s CPU as you can think of GPUs as lots of little CPUs working
together to achieve a common goal rather than one big CPU doing one thing at a
time. The CPU works well when you've got one single big task to do, but lots of
computing tasks in the past few decades have been on working to split big tasks
into individual independent tasks.

ML is one of those such tasks where each step requires the weights of hundreds
of tensors on a neural net to be adjusted. Using the CPU for this means that the
CPU is iterating over every tensor, adjusting the weights as it goes. The CPU is
fast, but this still takes time. The individual cores in a GPU are much slower,
but there are hundreds of them and they can update hundreds of tensors at a
time, speeding up the process by orders of magnitude.

I attempted to provision myself with a small GPU, to test some ML functionality
on this machine rather than the slower, cheaper machines and make sure that my
code had the right stuff installed to work with the GPUs. However, an error
came back, something along the lines of, “You do not have the necessary quota
to do this‥ go to the quota panel”. I therefore found this configuration panel
and saw that I had a quota of zero GPUs allowed. Surely another case of
granting myself permission! I upped my GPU quota from zero to a grand total of
one and was prompted for a reason. Ah, more bureaucracy! This reason will
surely just be sent back to me to grant myself permission! I put some silly
excuse like “i need gpu pls x” and clicked submit.

My inbox pinged:

> Hello,
>
> Thank you for contacting Google Cloud Platform Support.
>
> This message is to confirm that we've received your quota request for project
> 'candr-292514'. Quota increase requests typically take two business days to
> process. If this quota request is very urgent, please respond to this email so
> that our support agents can have full context when processing your quota
> increase request.

Onosecond. This doesn’t go to me. This goes to a real person at Google. A few
agonising minutes went by while I looked to see (unsuccessfully) if I could
cancel the request. Another e-mail: denied (I wouldn’t grant myself access
either). I had to wait two days to make another request.

So, 48 hours later, I wrote a much more formal paragraph apologising for my
previous request, outlining my humble request for a GPU to do some ML, pretty
please. Clicked submit and just as quickly, denied. Rude, maybe they were still
peeved about my previous communiqué? They don’t provide a reason, so I reached
out to them asking why my request was denied (conveniently omitting my blunder
two days earlier). I was told that I had “insufficient service usage history”.
In other words, I was still a free trial baby and hadn’t put down any megabucks
on their services already, so they ~~couldn’t~~ wouldn’t give me one.

# Options

I can’t have moved all my infrastructure to Google for nothing, and I was happy
about the rest of the service, so I pushed another route further. I received a
much more helpful e-mail from someone who outlined my options going forward:

1. Reapply for quota
2. Apply for research credits
3. Reach out to certified partners

Option one was an inevitability. I have not yet reapplied, but am hoping that my
continued usage of the platform will eventually allow me to use their hallowed
services.

Option two is interesting. It turns out that Google offer free usage of their
platform for research,[^6] but such credits must be applied for (they are
currently, of course, prioritising projects related to COVID-19) and I will be
applying to get more free usage that way. Their form asks for a link to my
research profile, so I took the time to update my University page with things
that make me look much more successful and qualified. I will be applying for
these credits as soon as I can think of the right words to write. There is also
the possibility of applying for a research grant through the University.

Option three is not useful: it is a list of companies that offer particular
services using the same platform, none of which were useful to me as I am
running bespoke services.

For now, and until Google deign to allow me to use their service further, all is
not lost. I can still use the platform as I was before, and I can use the same
tools (Docker/Kubernetes/etc.) to run those same ML tasks on my computer. It’s
not as cool or :sparkles: automatic :sparkles: magic, but I’m getting close to
the point where my own computer pretends to be the big computer in the sky with
the big GPU (although the big GPU is my own, or more precisely borrowed from my
brother who wasn’t using it).

I think that the main thing to keep in mind as I struggle to get to grips with
these technologies and ideas is that I am learning as I am going: six months ago
every technology listed here was alien to me, although I am looking forward to
getting over this technology hump and getting back to the musicology and
manuscripts.

[^1]: ‘Onosecond’, *Urban Dictionary* <[https://www.urbandictionary.com/define.php?term=onosecond](https://www.urbandictionary.com/define.php?term=onosecond)> [accessed 11 December 2020]

[^2]: Oz Nova, ‘You Are Not Google’, *Bradfield Blog* <[https://blog.bradfieldcs.com/you-are-not-google-84912cf44afb](https://blog.bradfieldcs.com/you-are-not-google-84912cf44afb)> [accessed 11 December 2020] as well as the discussion on Hacker News <[https://news.ycombinator.com/item?id=19576092](https://news.ycombinator.com/item?id=19576092)> [accessed 11 December 2020]

[^3]: Perhaps I should keep in mind the full quote: “We should forget about small efficiencies, say about 97% of the time: premature optimization is the root of all evil. Yet we should not pass up our opportunities in that critical 3%.” Donald Knuth, ‘Structured Programming with goto Statements’, *Computing Surveys*, vol. 6, n. 4 (1974), p.268 <[http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.6084&rep=rep1&type=pdf](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.6084&rep=rep1&type=pdf)> [accessed 11 December 2020]

[^4]: An example being the CI configuration for the CANDR website, which builds the website <[https://gitlab.com/candr1/candr/-/blob/master/.gitlab-ci.yml](https://gitlab.com/candr1/candr/-/blob/master/.gitlab-ci.yml)> using the Dockerfile <[https://gitlab.com/candr1/candr/-/blob/master/Dockerfile](https://gitlab.com/candr1/candr/-/blob/master/Dockerfile)> which is based on a base image made in this Dockerfile <[https://gitlab.com/candr1/candr-base-dockerfiles/candr-base-dockerfile/-/blob/master/Dockerfile](https://gitlab.com/candr1/candr-base-dockerfiles/candr-base-dockerfile/-/blob/master/Dockerfile)> [accessed 11 December 2020]

[^5]: Joshua Stutter, ‘CANDR ML Runner’, *GitLab* <[https://gitlab.com/candr1/candr-ml-runner](https://gitlab.com/candr1/candr-ml-runner)> [accessed 11 December 2020]

[^6]: ‘Research Credits’, *Google* <[https://edu.google.com/programs/credits/research/](https://edu.google.com/programs/credits/research/)> [accessed 11 December 2020]
