---
title: "Graph–based Analysis"
permalink: false
date: 2022-03-25 15:01:15 +0
---

The long slog of inputting both W1 and W2 into CANDR is complete. You can view
the result [here](https://www.candr.tk/browse/source/).[^1] F, although its
facsimiles have been uploaded to the site, is for now lying with only its system
and stave extents partially defined, and work on the tracing–transcription of F
is not begun properly. This is simply due to time constraints. Perhaps F, too,
will be encoded in the near future. What we do have however, is a database
containing the positional data of the musical notation of both W1 and W2. This
alone is something that I am proud of (the database file currently weighing in
at 231MB),[^2] but the most important issue is how to analyse this data in order
to extract features that will help me to answer my research questions.

# Domains

One distinction that I find particularly useful from MEI is that of musical
*domain*. MEI correctly realises that there is no “one encoding to rule them
all” and provides tools and schemata for encodings that consider differences of
opinion, inconsistencies, and elements of performance practice. MEI accomplishes
this through the concept of simultaneous musical domains. MEI acknowledge SMDL
(the *Standard Music Description Language*) as the genesis of this idea,[^3]
formalising the concept of musical domain from Milton Babbitt.[^4] SMDL divides
the area of musical domain into four separate categories (definitions mine):

- Logical domain: the “composer’s intentions”
- Gestural domain: aspects of performance
- Visual domain: how notation looks on the page
- Analytical domain: post hoc analyses of music

MEI shares the same terminology of domain with SMDL, but freely admit that they
prioritise ‘the visual domain over the gestural domain by (partly) conflating
the logical and the visual domains’.[^5] There is a certain inconsistency in the
formulation of the logical domain carried over from Babbitt’s 1965-era
musicological thought (although I do applaud him for opening his writing by
admitting ‘I am not a computer expert, and I am not a musicologist’).[^6] SMDL
defines the logical domain as ‘the basic musical content’ without first defining
what falls exactly under “basic”:

> The logical domain is describable as “the composer’s intentions with respect
> to pitches, rhythms, harmonies, dynamics, tempi, articulations, accents, etc”
> … It can also be described as “the abstract information common to both the
> gestural and visual domains.”[^7]

What unknown source the SMDL specification is quoting exactly notwithstanding,
SMDL leans heavily into a problematic concept of a commonly–understood authorial
intent between what is written on the page and what is performed. Rather it is
precisely those ‘pitches, rhythms, harmonies, dynamics, tempi, articulations,
accents, etc.’ that are the subject of discussion and should, in my view, not
form part of a “logical” domain of infallible truth, the implicit understanding
being that anything falling outside the “logical” domain must be “illogical”.

MEI are somewhat correct therefore to ‘conflate’ — although “conflate” seems a
little unfair to a decision that clearly understands the issue above — the
logical and visual domains, but in my view they uphold the same problematic
basis. MEI clearly prioritises visual aspects of notation above all else.
Indeed, much of MEI’s apparata are geared towards encoding Western notation:
pitches are referred to by their position in the twelve–note octave rather than,
for example, by their absolute frequency, there are multiple attributes for
measuring distances and lengths between objects on a page, definitions of music
begin primarily with how they are divided into systems and staves on a page. It
is difficult to escape the confines of common Western music notation when
encoding in MEI. Indeed, the gestural and analytical often appear as
afterthoughts to a primarily visual–logical encoding in MEI.

# Semantic Domain

Using this same terminology of domain, CANDR has so far been encoding solely in
the visual domain. Not to be confused with MEI’s conflated visual–logical domain
where the position of an item is attached to its semantic interpretation in a
musical syntax, but instead a purely visual encoding where I have encoded simply
*what* an element is and *where* it is. Each element on a stave could be
simplified down to a tag plus a set of cartesian coordinates describing the
element’s absolute position or even its extent. For example, a note and a clef
could be encoded in a cod-XML as:

	<candr:note x="420" y="69"/>
	<candr:clef x="8008" y="1337" width="86" height="42" angle="0"/>

Such a domain has its uses: using this data I could very easily compare the gaps
between elements on a stave, or the size of those elements and answer questions
regarding the scribal identities and practices present in the MSS. Although
interesting questions, this would bring me no closer to finding out the
constitution and construction of *clausulae*, which are fundamentally a musical
construct. The data in its raw form says nothing as to what pitch the note is or
what stave line the clef sits on. This data must therefore be transferred into a
different domain.

Rather than the term “logical domain” which for me carries too many formalist,
positivist connotations — a domain to be useful in an analysis which supposes to
include all possible valid interpretaions — I prefer to use the term “semantic”.
A semantic domain — the indefinite article implying the existence of other
semantic domains — is an interpretation of meaning (one of perhaps many) from
the visual domain. Rather than assuming a one–to–one mapping of the visual onto
its meaning, I envisage a number of possible readings based upon just as many
tunable parameters: *who* (or what) is doing the interpretation, *how* (by what
process) and *why* (for what purpose)?

# Automatic Transcription

To convert from this pure visual domain into the semantic (musical) domain, I
developed two modules on the CANDR site: a transcribing module that takes a
stave or system and infers from its placement of stave items their semantic
attributes (for example linking a note to a stave line and a clef to infer its
pitch), and a walking module that moves over the first–level schema of the
sources, folios and facsimiles to feed the transcribing module staves in order.

## Transcribing Module

All transcribing modules inherit from an `AbstractTranscriber` class,[^8] which
defines the basic methods for transcribing a database item, such as fetching the
items from the database and support for beginning a transcription partway
through a stave. It is up to inheriting classes to implement exactly how the
transcription is carried out and what items are fetched from the database. For
example, `TextTranscriber` (for transcribing the words of a stave) implements
the function for retrieving items from the database thus (altered and commented
for clarity):[^9]

	protected function get_items($record) {
		// "record" is stave, so a system is its "up"
		$system = $record->up();

		// get all links in this system
		$links = $system->links()->has('setting')->get();

		// get all the items associated with each link
		$link_items = $links->flatMap(function($a) {
			return $a->items;
		})->filter(function($a) use (&$record) {
			return $a->up->is($record);
		});

		// get all the items on this stave only
		// filter them to only be syllables
		$items = $record->items()
			->with('type')
			->whereIn(
				'itemtype_type',
				[
					'App\Models\Syllable',
					'App\Models\EditorialSyllable'
				]
			)->get();

		// create a new store of notes
		$note_store = new MEINoteStore();

		// get all notes on this stave
		// add them to the note store
		$record->items()
			->with('type')
			->where('itemtype_type', 'App\Models\Note')
			->get()->map(function($a) use (&$note_store) {
				$type = $a->type;
				$note_store->add_note(
					$type->comp_x,
					$type->comp_y,
					$type
				);
			});

		// create a unique set of all items on the stave,
		// including link items
		$syllables = [];
		$seen = new \Ds\Set();
		$all_items = $link_items
			->merge($items)
			->reject(function ($a) use (&$seen) {
				$reject = $seen->contains($a->id);
				if($reject) {
					$seen->add($a->id);
				}
				return $reject;
			});

		// iterate through all the items, filtering by syllable
		foreach($all_items as $item) {
			$syllable = $item->type;
			$item_class = get_class($syllable);
			if(
				$item_class === 'App\Models\Syllable' ||
				$item_class === 'App\Models\EditorialSyllable'
			) {
				// using the note store, find the nearest note
				// to this syllable

				$nearest_note = $note_store->nearest_note(
					$syllable->centrepoint_x,
					$syllable->centrepoint_y
				);
				if($nearest_note) {
					// +1 to shift it right ever so slightly
					$syllable->centrepoint_x = 
						$nearest_note->comp_x + 1;
					$syllable->centrepoint_y =
						$nearest_note->comp_y;
				}
			}
			
			// add to the list of syllables
			$syllables[] = $syllable;
		}
		
		// sort the items using a custom function, left to right
		usort($syllables, function($a, $b) {
			$c = $a->comp_x <=> $b->comp_x;
			if($c) {
				return $c;
			}
			return $b->comp_y <=> $a->comp_y;
		});
		return $syllables;
	}

In essence, this function not only retrieves the stave’s syllables from the
database, but also ensures that they occur at the right place to be included in
a synchronisation link by moving them just right of the nearest note, and sorts
them left to right to be transcribed. `TextTranscriber` is a small
proof–of–concept for the transcription element, but CANDR also includes an
`MEITranscriber` implementation following the same principles. To transcribe
music rather than syllables is more complex, but `MEITranscriber` returns MEI
(in a CANDR–specific dialect) for a system of music (the limits of which will be
discussed later).

## Walking Module

Similarly, all walking modules inherit from an `AbstractWalker` class,[^10]
whose main function `walk()` begins at any point in a source and walks forward,
calling the transcriber, until it reaches any one of many “stop” elements,
passed in as an array. For example, given a stave with two elements,
respectively listed as “start” and “stop”, the walker will call the transcriber
once on that stave with those parameters. However, if the “stop” elements are on
another stave, then the walker will call itself recursively on the element
above: walking on a stave without raising a “stop” signal will cause the walker
to then find the common system and transcribe the next stave in that system. If
the “stop” is not found there either, it will go up to the facsimile or even
folio level and keep transcribing until that “stop” element is found. The
`TextWalker` simply has to instantiate a `TextTranscriber` as its transcribing
class, and transcribe only the lowest stave (where the words are found) in a
system.[^11] Once again, there is an `MEIWalker` for creating MEI
documents.[^12]

<figure><img src="images/220325/walker.png"/><figcaption>How a walking module walks between first–level schema items</figcaption></figure>

## Result

These two elements combine to create automatic transcriptions of the sources
into words and MEI. For example, navigating to
[https://candr.tk/browse/facsimile/86/](https://candr.tk/browse/facsimile/86/)
will show you the facsimile of folio 43v in W2. If you alter that address to
[https://candr.tk/browse/facsimile/86/text/](https://candr.tk/browse/facsimile/86/text/),
then it will display the transcribed text for that facsimile “regina tu laquei
tu doloris medicina fons olei uas honoris Tu uulneris medelam reperis egris
efficeris oleum unctionis Post ueteris quem”. Requesting
[https://candr.tk/browse/facsimile/86/mei/](https://candr.tk/browse/facsimile/86/mei/)
will yield an MEI document of the music on that page.

Behind the scenes, there is a fair amount of caching to speed up this process.
Every time a transcription is requested, CANDR first checks a cache record in
the database. If there is a cache hit then it simply returns that cache record.
This means that requesting the same transcription twice should not re-transcribe
the item, needlessly hitting the database, but returns a pre-computed record.
However, if a database element is altered after a transcription is cached then
the transcription may be invalid. CANDR notifies all the records that use that
element that their transcription may now be invalidated. For example, if a stave
has been altered then that change is propagated throughout the cache system:
caches for its stave, facsimile and folio are instantly deleted such that the
next time the facsimile transcription is retrieved, it must be re-transcribed
using the most up–to–date information. This way we can ensure that the caches
are never out of date.

# The “Setting”

Considering once again the musical domains of the visual vs the semantic, the
transcriptions created so far have themselves considered only the visual aspects
of the source. We can transcribe staves, systems, facsimile and folios but, more
often than not, music flows from one system to the next and from one folio to
another. We cannot simply conceive of a source as a disparate set of
codicological items, but as a book intended to be read from one opening to the
next. The concept of the walker allows for this movement between codicological
items as previously described, but it needs to know where to start and stop. How
do we find elements to begin and end our transcription? I would like to
introduce another concept into this ontology: that of the musical “setting”.

A setting is a semantic rather than visual construct, but relies on visual cues
supplied by an editor (completed during the data input stage). I have previously
mentioned my concept of the synchronisation link which indicates that two
elements on adjacent staves occur simultaneously with one another. The beginning
of a piece of music links items from all staves together, and this special kind
of link is tagged as being a “setting”. Using these tags, a walker can walk from
one tag to the next, returning the music of a setting.

However, these are also tagged and titled by hand, being an editorial creation.
It is here that CANDR shades into replication of some of the functionality of
DIAMM. DIAMM lists the contents of W1 as a hand–curated inventory (human
mistakes included!), listing “compositions” with their titles, “composers”
(nearly all of whom are anonymous) and start/stop folios.[^13] However, DIAMM
considers pieces such as *Alleluya. Nativitas gloriose* as single items, whereas
I treat them as separate alleluia and verse settings. Regardless, these settings
can be categorised and listed in CANDR at
[https://www.candr.tk/browse/setting/](https://candr.tk/browse/setting/). The
advantage that CANDR brings is the possibility of then browsing the facsimiles
for that setting, and viewing a transcription of the words and music, all the
way to the granularity of each note.

This is the effective completion of the online database stage of CANDR. The next
analytical stages are completed offline by scraping data from the site.

# Encoding Encapsulation

MEI is a hierarchical encoding, by virtue of its tree–based XML format. An MEI
document (after header and boilerplate) begins with a base music element, such
as `<music>` and then moves into that music’s components in a divide–and–conquer
strategy until it reaches a level where atomic elements can be encoded directly
without any children. Looking at MEI’s example encoding of Aguardo’s *Walzer*
from the MEI sample encodings repository, lines 227–254 begin the encoding
proper by limiting the initial scope until we reach the first moment of the
first stave, a quaver rest.[^14] Greatly simplifying those lines to just contain
the initial rest, we get a tree structure:

	<music>
		<body>
			<mdiv>
				<score>
					<scoreDef>
						…
					</scoreDef>
					<section>
						<measure>
							<staff>
								<layer>
									<rest/>
								</layer>
							</staff>
						</measure>
					</section>
				</score>
			</mdiv>
		</body>
	</music>

This XML structure can also be visualised as a series of concentric boxes,
similar to the CSS box model:

<figure><img src="images/220325/mei-structure.png"/><figcaption>This blog post is full of pretty pictures</figcaption></figure>

Each element can only be linked to other elements in three, mutually exclusive
ways:

1. Parent: the containing box of an element
2. Children: the boxes contained within this element
3. Siblings: other boxes that share the same parent element

The `<section>` in this example has one parent `<score>`, one sibling
`<scoreDef>` and five children: `<measure>` as well as others. These limits are
often powerful, as they enforce a strict design philosophy on a document such as
MEI. Elements are contained within other elements such that an element can only
have one parent. For example, a `<measure>` can only occur in one `<section>` at
a time. This makes parsing and analysing this data structure (a tree) simple as
it is guaranteed to be acyclic (i.e. when descending the structure it is
impossible to visit an element twice).

Generally this works well for MEI as it is easy to conceive of musical structure
as such: rests in layers in staves in measures in sections in scores in
divisions in a musical body, and this does work well for for elements that
occupy a single space in time, such as simple notes. On the whole it echoes how
common practice Western music notation is the de facto standard for encoding and
how music must be fit into its confines: for example notes are contained within
measures and cannot simply overflow a bar.

How, then, might we encode Notre Dame notation using such a model? Before
thinking of MEI exactly, let us consider it simply as a series of boxes. It is
simple to draw boxes around a system and its staves and also around smaller
items such as notes and ligatures, but in the midground we come across a thorny
issue: do we first divide a system into its constituent staves or into a series
of virtual timeseries boxes (which we could errantly name “ordines”)? Each has
its advantages and disadvantages, and the issue is roughly analogous to
MusicXML’s two encodings: timewise and partwise, either encoding measures of
music at–once or encoding entire parts or voices at–once. MEI is always
equivalent to MusicXML’s partwise encoding.

<figure><img src="images/220325/drawing-boxes.png"/><figcaption>Two attempts to draw boxes around elements: top is partwise (staves first) and bottom is timewise (“ordines” first)</figcaption></figure>

<figure><img src="images/220325/partwise-timewise.png"/><figcaption>The above conceptualised as concentric boxes</figcaption></figure>

## Partwise

- Advantages:
  1. Each part element is a sibling of its successor and predecessor.
  2. Staves can be considered at–once.
  3. Mirrors the exact lack of verticality in the notation.
- Disadvantages:
  1. The information of which items occur simultaneously is lost. In common MEI
  or MusicXML, this can be inferred by counting durations, but in Notre Dame
  notation, rhythm is subjective and up to interpretation. We cannot rely on
  duration counting.
  2. Virtual items, such as the idea of a common tactus between parts, or a
  common ordo length, is also lost.

## Timewise

- Advantages:
  1. Everything that occurs together is grouped together.
  2. Polyphony can be easily extracted
- Disadvantages:
  1. Often to reach the next note in a stave, we have to traverse up the
  structure to reach a common ancestor.
  2. Infers an editorial synchronisation between parts as a first–class element.
  3. Cannot infer verticality between a subset of parts.

MEI’s solution to this is to keep the music encoded partwise, but use a `@synch`
attribute to break the encapsulation and link between other items in the
document.[^15] I use this exact solution to encode Notre Dame music in MEI, but
the data model is still unsatisfactory. There is a false idea of musical
encapsulation with virtual objects (such as ordines) which is then arbitrarily
broken to make connections that are not formalised in the model. This also makes
parsing the data much more difficult: more than simply viewing a node and the
limited relationships between parents, siblings and children, a parser must now
look at the node’s attributes and other objects in the tree, potentially
creating cycles. In short, it seems like a bit of a hack. In terms of the
structure, what happens if an element has two parents? In which “box” should it
belong?[^16]

<figure><img src="images/220325/linking.png"/><figcaption>How I link Notre Dame notation together using the synch attribute in MEI</figcaption></figure>

# A third way: graph(-wise?)

In fact, MEI provides a whole selection of attributes in the `att.linking` class
to indicate relationships between items this way, such as `@next` and `@prev`
for temporal relationships. However, these still must be structured in the
`<music>` element. Rather than encapsulation, this music that falls outside
common Western music boxes would be better encoded with links between items
being first class, in order words encoded entirely as linked data as a directed
graph. These kinds of graphs are not of the bar, line, pie, scatter ilk but
rather those of graph theory, what we might commonly term networks. Instead of
items preceding each other being known by the order of siblings, and items in
ligature being known by being children of a “ligature” element, each element
would link from one to the next, and an element in a ligature would have a link
signifying membership of a ligature. Synchronisation links are therefore simply
another link of the same class as temporal relationships or ligature membership.

We could for instance model this as RDF data. Consider the same music as linked
data (in Turtle format, using example namespaces for simplicity):

	@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
	@prefix ex: <http://www.example.com/notre-dame/> .
	@prefix it: <http://www.example.com/items/> .

	<http://www.example.com/items/note/1/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:next <http://www.example.com/items/ligature/1/> ;
		ex:synch <http://www.example.com/items/note/21/> .
	<http://www.example.com/items/ligature/1/>
		ex:ligatureType <http://www.example.com/notre-dame/ligature-types/Square/> ;
		ex:next <http://www.example.com/items/divisione/1/> ;
		ex:prev <http://www.example.com/items/note/1/> .
	<http://www.example.com/items/note/2/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:inLigature <http://www.example.com/items/ligature/1/> ;
		ex:next <http://www.example.com/items/note/3/> .
	<http://www.example.com/items/note/3/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/E/> ;
		ex:inLigature <http://www.example.com/items/ligature/1/> ;
		ex:next <http://www.example.com/items/note/4/> ;
		ex:prev <http://www.example.com/items/note/2/> .
	<http://www.example.com/items/note/4/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:inLigature <http://www.example.com/items/ligature/1/> ;
		ex:prev <http://www.example.com/items/note/3/> .
	<http://www.example.com/items/divisione/1/>
		ex:prev <http://www.example.com/items/ligature/1/> ;
		ex:next <http://www.example.com/items/note/5/> ;
		ex:synch <http://www.example.com/items/divisione/5/> .
	<http://www.example.com/items/note/5/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:prev	<http://www.example.com/items/divisione/1/> ;
		ex:next <http://www.example.com/items/divisione/2/> .
	<http://www.example.com/items/divisione/2/>
		ex:prev <http://www.example.com/items/note/5/> ;
		ex:next <http://www.example.com/items/note/6/> .
	<http://www.example.com/items/note/6/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/G/> ;
		ex:prev <http://www.example.com/items/divisione/2/> ;
		ex:next <http://www.example.com/items/note/7/> .
	<http:/www.example.com/items/note/7/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:prev <http://www.example.com/items/note/6/> ;
		ex:next <http://www.example.com/items/note/8/> .
	<http://www.example.com/items/note/8/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/G/> ;
		ex:prev <http://www.example.com/items/note/7/> ;
		ex:next <http://www.example.com/items/note/9/> .
	<http://www.example.com/items/note/9/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/A/> ;
		ex:prev <http://www.example.com/items/note/8/> ;
		ex:next <http://www.example.com/items/note/10/> .
	<http://www.example.com/items/note/10/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/A/> ;
		ex:hasPlica <http://www.example.com/notre-dame/plicae/Up/> ;
		ex:prev <http://www.example.com/items/note/9/> ;
		ex:next <http://www.example.com/items/divisione/3/> .
	<http://www.example.com/items/divisione/3/>
		ex:prev <http://www.example.com/items/note/10/> ;
		ex:next <http://www.example.com/items/ligature/2/> ;
		ex:synch <http://www.example.com/items/divisione/6/> .
	<http://www.example.com/items/ligature/2/>
		ex:ligatureType <http://www.example.com/notre-dame/ligature-types/Currentes/> ;
		ex:prev <http://www.example.com/items/divisione/3/> ;
		ex:next <http://www.example.com/items/ligature/3/> .
	<http://www.example.com/items/note/11/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/c/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:next <http://www.example.com/items/note/12/> .
	<http://www.example.com/items/note/12/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/B/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/11/> ;
		ex:next <http://www.example.com/items/note/13/> .
	<http://www.example.com/items/note/13/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/A/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/12/> ;
		ex:next <http://www.example.com/items/note/14/> .
	<http://www.example.com/items/note/14/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/G/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/13/> ;
		ex:next <http://www.example.com/items/note/15/> .
	<http://www.example.com/items/note/15/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/14/> ;
		ex:next <http://www.example.com/items/note/16/> .
	<http://www.example.com/items/note/16/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/E/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/15/> ;
		ex:next <http://www.example.com/items/note/17/> .
	<http://www.example.com/items/note/17/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:inLigature <http://www.example.com/items/ligature/2/> ;
		ex:prev <http://www.example.com/items/note/16/> .
	<http://www.example.com/items/ligature/3/>
		ex:ligatureType <http://www.example.com/notre-dame/ligature-types/Square/> ;
		ex:prev <http://www.example.com/items/ligature/2/> ;
		ex:next <http://www.example.com/items/note/20/> .
	<http://www.example.com/items/note/18/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:inLigature <http://www.example.com/items/ligature/3/> ;
		ex:next <http://www.example.com/items/note/19/> .
	<http://www.example.com/items/note/19/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/E/> ;
		ex:inLigature <http://www.example.com/items/ligature/3/> ;
		ex:prev <http://www.example.com/items/note/18/> .
	<http://www.example.com/items/note/20/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:prev <http://www.example.com/items/ligature/3/> ;
		ex:next <http://www.example.com/items/divisione/4/> .
	<http://www.example.com/items/divisione/4/>
		ex:prev <http://www.example.com/items/note/20/> ;
		ex:synch <http://www.example.com/items/divisione/7/> .
	<http://www.example.com/items/note/21/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/C/> ;
		ex:next <http://www.example.com/items/note/22/> ;
		ex:synch <http://www.example.com/items/note/1/> .
	<http://www.example.com/items/note/22/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:prev <http://www.example.com/items/note/21/> ;
		ex:next <http://www.example.com/items/divisione/5/> .
	<http://www.example.com/items/divisione/5/>
		ex:prev <http://www.example.com/items/note/22/> ;
		ex:next <http://www.example.com/items/ligature/4/> ;
		ex:synch <http://www.example.com/items/divisione/1/> .
	<http://www.example.com/items/ligature/4/>
		ex:prev <http://www.example.com/items/divisione/4/> ;
		ex:next <http://www.example.com/items/note/26/> .
	<http://www.example.com/items/note/23/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/G/> ;
		ex:inLigature <http://www.example.com/items/ligature/4/> ;
		ex:next <http://www.example.com/items/note/24/> .
	<http://www.example.com/items/note/24/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/A/> ;
		ex:inLigature <http://www.example.com/items/ligature/4/> ;
		ex:prev <http://www.example.com/items/note/23/> ;
		ex:next <http://www.example.com/items/note/25/> .
	<http://www.example.com/items/note/25/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/C/> ;
		ex:inLigature <http://www.example.com/items/ligature/4/> ;
		ex:prev <http://www.example.com/items/note/24/> .
	<http://www.example.com/items/note/26/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/F/> ;
		ex:prev <http://www.example.com/items/ligature/4/> ;
		ex:next <http://www.example.com/items/note/27/> .
	<http://www.example.com/items/note/27/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/E/> ;
		ex:prev <http://www.example.com/items/note/26/> ;
		ex:next <http://www.example.com/items/note/28/> .
	<http://www.example.com/items/note/28/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:prev <http://www.example.com/items/note/27/> ;
		ex:next <http://www.example.com/items/note/29/> .
	<http://www.example.com/items/note/29/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:prev <http://www.example.com/items/note/28/> ;
		ex:next <http://www.example.com/items/divisione/6/> .
	<http://www.example.com/items/divisione/6/>
		ex:prev <http://www.example.com/items/note/29/> ;
		ex:next <http://www.example.com/items/note/30/> ;
		ex:synch <http://www.example.com/items/divisione/3/> .
	<http://www.example.com/items/note/30/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/C/> ;
		ex:prev <http://www.example.com/items/divisione/6/> ;
		ex:next <http://www.example.com/items/note/31/> .
	<http://www.example.com/items/note/31/>
		ex:hasPitch <http://www.example.com/notre-dame/pitches/D/> ;
		ex:prev <http://www.example.com/items/note/30/> ;
		ex:next <http://www.example.com/items/divisione/7/> .
	<http://www.example.com/items/divisione/7/>
		ex:prev <http://www.example.com/items/note/31/>	;
		ex:synch <http://www.example.com/items/divisione/4/> .

This is a big leap, and understandably much more difficult to conceptualise than
the box model, so it can be visualised as an extraordinarily complex digraph
(using
[https://www.ldf.fi/service/rdf-grapher](https://www.ldf.fi/service/rdf-grapher)
for visualisation):

<figure><img src="images/220325/rdf-grapher.png"/><figcaption>Very large image, zoom in or open image in new tab for detail</figcaption></figure>

To understand this better, let’s look at the node “note/1/” (it is on the
centre–left of the visualisation). It has three links going out and two coming
in.

<figure><img src="images/220325/rdf-subgrapher.png"/></figure>

The first, `ex:hasPitch` indicates that this note has a pitch of F. It then has
two links, one going out `ex:next` and one coming in `ex:prev`, indicating that
the note’s next item is “ligature/1/”. “ligature/1/” also has a link indicating
that its previous item is “note/1/”. Finally, “note/1/” is linked using
`ex:synch` with “note/21/” in both directions, indicating that these two items
occur simultaneously. This is true throughout the graph (although the
visualisation tool struggles with the amount of links), so that we can model the
data consistently without needing to place the items in boxes (encapsulation).
Traversal of this data structure is much more simple, all that we need to do is
to examine an item’s links to see how it relates to other items. For example, we
could find all the notes with pitch F by examing the “ex:pitches/F/” node and
following the links backward (backtracking).

We can use this graph structure (although not in RDF for reasons I will explain
later) to analyse the same music.

# Creating an Analysis Package

*Candr-analysis* is a Python package for analysing a Notre Dame notation corpus
(although it could be further generalised to more notational forms).[^17] It
consists of multiple modules that can be linked together to: fetch information
(such as from the CANDR site), parse notation, convert a tree–based
representation into a graph, transform that graph into subgraphs, link elements
in graphs together, and generate analyses based upon those graphs.

## Pipeline

*Candr-analysis* is based upon a data pipeline architecture, where analysis
modules are linked together to asynchronously pass data from one stage to the
next. The act of creating an analysis is to link pipeline modules together so
that the output from one feeds the input of the next.

The root of this is the `Pipeline` class, which receives `PipeObject`s (or
rather objects that inherit from `PipeObject`) and links them together.[^18]
Once all the objects have been added to the pipeline, then the pipeline is
instructed to begin running and wait for the objects to run. Each object runs in
its own process, so a resource–intensive component such as graph creation does
not necessarily slow down another component such as parsing. This makes creating
new analyses simple. For example, an initial stage of text analysis is to fetch
all the text transcriptions from the CANDR site. This is achieved by simply
running this small script:

	from candr_analysis.scrapers.candr.rdf.setting import Setting as Scraper
	from candr_analysis.transcribers.candr.text import Setting as Transcriber
	from candr_analysis.utitilities.pipeline import Pipeline
	from candr_analsysis.utilities.progress import CLIProgress as Progress
	from candr_analysis.transformers.pickle import PickleSaver as Saver

	progress = Progress()
	pipeline = Pipeline()
	pipeline.add(Scraper(progress=progress))
	pipeline.add(Transcriber(progress=progress))
	pipeline.add(Saver('output.pkl', flush=True))
	pipeline.start()
	pipeline.stop()

It is clear to see from this fully–functional program that it accomplishes three
tasks:

1. Setting up a pipeline;
2. Adding the components of the website scraper, transcriber and a component that
saves the result to disk;
3. Running the pipeline and waiting for it to finish.

Pipelines of any complexity can be set up this way which allows for a dynamic
iteration of analysis ideas, and the ability to save results to disk at any
stage allows for analyses to be created, saved, resumed, re-run, and
meta-analyses to be created.

# PipeObjects

Adding new functionality to *Candr-analysis* is as simple as inheriting from the
base class of `PipeObject` and defining a generator function `process()` to take
in an input and returning sets of output to be passed to the next object in the
pipeline. Below are some objects that I have already created for my uses:

## Scraping

A “scraping” `PipeObject` is one that views a data source, such as the CANDR
site, and generates metadata based upon that source. For example, the
`rdf.setting` scraper views the setting list page and uses its RDF markup to
create a rich list of settings in the sources.

## Transcribing

A “transcribing” `PipeObject` takes a single data endpoint and fetches an
individual transcripton of that point. For example, the `candr.text` transcriber
receives a CANDR endpoint object and fetches its respective text transcription.
Similarly the `candr.MEI` transcriber fetches the MEI transcription.

## Parsing

A “parsing” `PipeObject` takes an unparsed input, such as one from the CANDR
site, and parses it into something more useful for further analysis. There can
be more than one parser for any input, for example a parser may only be
interested in certain subsets of the data. Text generally needs no parser, but
the MEI parser validates and transforms an MEI file into a graph–based
representation as outline above. The MEI parser defines multiple relationships
(edge types in a multi–digraph, roughly analogous to the data model of RDF)
between items. These have been generalised so that they could be used with any
repertory of music, not just Notre Dame notation, and the graph can support more
kinds of links:

- Sequential: B follows A
- Synchronous (Hardsynch): A and B occur at the same time
- Softsynch: A and B are some distance away, but their distance can be computed,
  the weight of this edge being defined as the reciprocal of the distance between
  the nodes.

## Transforming

A “transforming” `PipeObject` performs some sort of transformation on parsed
data before it is analysed. For example, data may need to be weighted correctly
or converted from one data structure to another.

## Analysing

An “analysing” `PipeObject` takes a corpus of data and performs some analysis on
it, generating some new artefact — typically a database — that can then be
interpreted.

## Interpreting

An “interpreting” `PipeObject` extracts useful information from an analysis.
What that information is depends greatly on the type of analysis being done.

## Utilities

A utility is some quality–of–life object that can help make the workflow
simpler. For example, I have found that a good workflow is to scrape and
transcribe in one step, taking all the necessary data from the network in one go
before moving onto following stages. The code above uses an object that dumps
its input directly to disk, and there is another object that can read data from
disk and send it into a new pipe. Examining these intermediary files is also
indispensable in debugging.

# Graph Analysis

What remains, then, is the analysis of these graphs. Although the narrative
inroad to Notre Dame notation as a graph was through RDF, *Candr-analysis*
internally uses the more general–purpose library *NetworkX* to generate its
graphs during parsing,[^19] and *SQLAlchemy* for on–disk graphs.[^20] The reason
for this is that although RDF’s model fits my data model and is simple to
comprehend, it is designed for linking humanistic data using strings. There is
therefore no RDF implementation efficient enough for this use case. For example,
I could use SPARQL to query an RDF store for this dataset, but the number of
nodes that will be created and the density of connections between the nodes
would likely be far too much for a sensible RDF implementation to handle.[^21]

## N-grams

A common methodology in computational linguistics is that analysing a text’s
n-grams, splitting texts into tokens then combining sequential tokens of size n.
In other words, creating 2-grams (bigrams) of “the cat sat on the mat” would
yield the following set: “the cat”, “cat sat”, “sat on”, “on the”, “the mat”.
3-grams (trigrams) of the same text would be “the cat sat”, “cat sat on”, “sat
on the”, “on the mat”. These can be used to detect similarities and syntax
between texts other than simply counting word occurrences (which would be
1-grams or unigrams). N-grams can also detect word sequences and context.
Generating n-grams for CANDR text transcriptions is as simple as defining a
`PipeObject` that returns n-grams for input strings:

	class TextNGrams(PipeObject):
		def _split(self, raw):
			return re.split(self._split_regex, raw)
		def process(self, input_data):
			split = list(filter(
				(lambda a: a != ''),
				self._split(input_data.transcription)
			))
			ret = list(zip(*[split[i:] for i in range(self._n)]))
			if ret:
				yield NgramsTranscriptionRecord(
					input_data.subject,
					input_data.type,
					ret
				)
			return

However, the more difficult task is that of generating n-grams of music as this
is a problem that has not yet been solved, indeed n-grams generally cannot be
generated for more than single streams of data. Monophonic music (such as chant)
could simply be tokenised into its constituent pitches, but polyphonic music
(such as the Notre Dame repertory) depends not simply on what pitches occur in
what order, but also what is happening in other voices at the same time. I
propose a method of extracting workable n-grams that are not represented by
tokens but by graphs, and maintain their synchronisation contexts to then be
analysed as a whole corpus graph.

Consider the same passage of music we looked at previously. We could more simply
represent it as a graph like so:

<figure><img src="images/220325/graph-ngram.png"/><figcaption>Simplified graph of music</figcaption></figure>

In the upper voice, there are four notes in the first ordo: F, F, E, D. In the
lower voice, there are only two: C, D. Not everything is synched together — only
the initial F to the initial C — so we don’t know exactly where the D comes in
the lower voice. We must therefore make some assumptions. Since there are half
as many notes, we can assume that in this voice the notes move at roughly half
the speed as the upper voice. The C in the lower voice must change to a D
somewhere around the E in the upper voice. This is likely incorrect, but over
the course of millions of grams, these assumptions should average out.

If we wanted to created unigrams of the first ordo, we could create:

F→C\
F→C\
E→D\
D→D

The next ordo (with only one note in the lower voice) would all be paired with
F:

|→|\
F→F\
G→F\
F→F\
G→F\
A→F\
A^→F

And so on. However, if we wanted to create bigrams, we could move with the same
ratio, but using pairs of notes. In the first ordo top voice, there are three
bigrams: FF, FE, ED. The lower voice only has one bigram: CD. Our graph bigrams
are therefore:

FF→CD\
FE→CD\
ED→CD

There are no trigrams, so we must overflow into the next ordo to create one:

FFE→CDF\
FED→CDF\
EDF→CDF\
DFG→CDF

By using the graph representation of the music, these n-grams cannot be confused
with other n-grams with other synchronisation patterns. This is because there
are also soft synchronisation (softsynch) links computed between elements of the
graph. Consider this polyphonic music:

<figure><img src="images/220325/single-ngram.png"/><figcaption>A simple contrived example</figcaption></figure>

This yields four bigrams: FF→C, FE→C, EF→C, FE→C. There is a duplicate bigram,
FE→C and this might incorrectly indicate that they are identical. The first
bigram is closer to its synchronisation C than the second and this should be
reflected in its n-gram. To solve this, the softsynchs are computed such that
every note is linked to every other by the reciprocal of its distance (here
shown in dashed lines). The first FE→C bigram has softsynchs of .5 and .33
respectively whereas the second has .25 and .2: the synchronisation link between
the voices in the second is weaker although the pitch content of the bigram is
identical.

## N-grams in a corpus graph

A n-gram `PipeObject` can output subgraphs of a graph of a passage of music as a
series of n-grams to then be processed further. In my analysis, the n-grams are
further split up into their constituent voices, taking the mean of their
synchronisation weights, but split into hard vs soft synchs. For example, the
first bigram in the above example, FF→C would yield two records: FF and C. These
two records are linked by a hard synch of 1 and a soft synch of 0.5. The next
bigram, FE→C links FE and C with a hard synch of 0 and a soft synch of 0.42 (the
average of 0.5 and 0.33). These links are also linked back to the “subject” of
the link, for example the setting that the n-grams were generated from. This
corpus graph is then stored on disk as an SQLite database with millions of
connections made between n-grams.

We can query this corpus database by comparing the graph to another piece of
music and how similar it is to edges in the graph. For example, another piece of
music may contain FE→A. This would match FE, but in our graph there is only
FE→C. A scoring function controls the weight of certain parameters in
calculating which grams more closely match the queried grams. We can tweak
parameters of the graph search to control for gram size, the strength of hard vs
soft synchs and “wrong synchs” such as the example just mentioned where there is
a synch from FE but to C rather than A.

By way of example, we could give hard synchs (which always have an edge weight
of 1) a scoring ratio of 10, and soft synchs a weight of 5. Wrong synchs are
weighted less, say 0.1. We calculate the difference between the bigrams as:

<figure><img src="images/220325/formula.png"/></figure>

Where Hw is the hard synch weight, Hr is the hard synch ratio, Sw is the soft
synch weight and Sr is the soft synch ratio. Let us imagine that we query the
graph with FE→C with a hard synch of 1 and a soft synch of 0.5. This would
match the first bigram more than the second as the score for the first bigram
would be:

<figure><img src="images/220325/calculation1.png"/></figure>

Whereas the second bigram’s score would be:

<figure><img src="images/220325/calculation2.png"/></figure>

However, if we query FE→A with a soft synch mean of 0.64, then this would score
0 on hard and soft synch, but if we calculate hard and soft synchs again with
the wrong synch and multiply by 0.1 to find the score:

<figure><img src="images/220325/calculation3.png"/></figure>

We repeat this calculation for all the grams in the queried music and keep a
cumulative score of subjects (such as settings). Subjects that score small
differences over many grams may be inferred to be similar to the queried music.
The overall idea of this analysis is not to generate a single, binary truth “X
is the same music as Y”, but a view on a repertory (I can see this methodology
generalised to other repertories of music) where, given certain parameters and a
particular viewpoint, two pieces of music display a high degree of concordance
saying some more along the lines of “X, when split into bigrams and weighted
using these synch parameters, is most similar to Y but also scores highly on Z
etc.”

[^1]: Joshua Stutter, ‘Source | List’, *CANDR*
<[https://www.candr.tk/browse/source/](https://www.candr.tk/browse/source/)>
[accessed 23 March 2022]

[^2]: Joshua Stutter, ‘production.sql', *GitLab*
<[https://gitlab.com/candr1/candr-data/-/blob/master/production.sql](https://gitlab.com/candr1/candr-data/-/blob/master/production.sql)>
[accessed 23 March 2022]

[^3]: And it is interesting to note that SMDL appears to have been a big push at
Glasgow:
[https://eprints.staffs.ac.uk/5056/1/20000805_ICMC_MuTaTeD_accepted.pdf](https://eprints.staffs.ac.uk/5056/1/20000805_ICMC_MuTaTeD_accepted.pdf)

[^4]: Milton Babbit, ‘The Use of Computers in Musicological Research’
*Perspectives of New Music* vol.3 (1965), pp.74–83

[^5]: ‘Introduction to MEI’, *Music Encoding Initiative*
<[https://music-encoding.org/guidelines/dev/content/introduction.html#musicalDomains](https://music-encoding.org/guidelines/dev/content/introduction.html#musicalDomains)>
[accessed 24 March 2022]

[^6]: *The Use of Computers*, p.74

[^7]: *Standard Music Description Language (SMDL)*, International Organization
for Standardization
<[http://xml.coverpages.org/smdl10743-pdf.gz](http://xml.coverpages.org/smdl10743-pdf.gz)>
[accessed 24 March 2022]

[^8]: Joshua Stutter, ‘AbstractTranscriber.php’, *GitLab*
<[https://gitlab.com/candr1/candr/-/blob/master/app/Transcribers/AbstractTranscriber.php](https://gitlab.com/candr1/candr/-/blob/master/app/Transcribers/AbstractTranscriber.php)>
[accessed 24 March 2022]

[^9]: Joshua Stutter ‘TextTranscriber.php’, *GitLab*
<[https://gitlab.com/candr1/candr/-/blob/master/app/Transcribers/TextTranscriber.php](https://gitlab.com/candr1/candr/-/blob/master/app/Transcribers/TextTranscriber.php)>
[accessed 24 March 2022]

[^10]: Joshua Stutter ‘AbstractWalker.php’, *GitLab*
<[https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/AbstractWalker.php]([https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/AbstractWalker.php)>
[accessed 24 March 2022]

[^11]: Joshua Stuter ‘TextWalker.php’, *GitLab*
<[https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/TextWalker.php](https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/TextWalker.php)>
[accessed 24 March 2022]

[^12]: Joshua Stuter ‘MEIWalker.php’, *GitLab*
<[https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/MEIWalker.php](https://gitlab.com/candr1/candr/-/blob/master/app/Walkers/MEIWalker.php)>
[accessed 24 March 2022]

[^13]: ‘D-W Cod. Guelf. 628 Helmst. (Magnus Liber Organi; W1)’, *DIAMM*
<[https://www.diamm.ac.uk/sources/870/#/inventory](https://www.diamm.ac.uk/sources/870/#/inventory)>
[accessed 24 March 2022]

[^14]: Maja Hartwig & Kristina Richts, ‘Aguado_Walzer_G-major.mei’, *GitHub*
<[https://github.com/music-encoding/sample-encodings/blob/main/MEI_4.0/Music/Complete_examples/Aguado_Walzer_G-major.mei#L227-L254](https://github.com/music-encoding/sample-encodings/blob/main/MEI_4.0/Music/Complete_examples/Aguado_Walzer_G-major.mei#L227-L254)>
[accessed 24 March 2022]

[^15]: ‘Analysis Markup and Harmonies: General Relationships between Elements’,
*Music Encoding Initiative*
<[https://music-encoding.org/guidelines/v4/content/analysisharm.html#analysisDescribingRelationships](https://music-encoding.org/guidelines/v4/content/analysisharm.html#analysisDescribingRelationships)>
[accessed 24 March 2022]

[^16]: This same breaking of encapsulation is found in how MEI must deal with
ties and slurs, although they use another weird attribute hack:
[https://music-encoding.org/guidelines/v4/content/cmn.html#cmnSlurTies](https://music-encoding.org/guidelines/v4/content/cmn.html#cmnSlurTies)

[^17]: Joshua Stutter, ‘CANDR Analysis’, *GitLab*
<[https://gitlab.com/candr1/candr-analysis](https://gitlab.com/candr1/candr-analysis)>
[accessed 24 March 2022]

[^18]: Joshua Stutter, ‘pipeline.py’, *GitLab*
<[https://gitlab.com/candr1/candr-analysis/-/blob/424a2b0162b8f4055f35dc387ce6638b2dea4add/src/candr_analysis/utilities/pipeline.py#L128](https://gitlab.com/candr1/candr-analysis/-/blob/424a2b0162b8f4055f35dc387ce6638b2dea4add/src/candr_analysis/utilities/pipeline.py#L128)>
[accessed 24 March 2022]

[^19]: Aric Hagberg, Dan Schult & Pieter Swart, *NetworkX*
<[https://networkx.org/](https://networkx.org/)>
[accessed 25 March 2022]

[^20]: Michael Bayer, *SQLAlchemy*,
<[https://www.sqlalchemy.org/](https://www.sqlalchemy.org/)>
[accessed 25 March 2022]

[^21]: ‘RDFlib isn’t really build [sic] for speed’. Gunnar Aastrand Grimnes,
‘Performance of graph.query over large local triple store’, *RDFLib Discussion*
<[https://github.com/RDFLib/rdflib/discussions/1563#discussioncomment-1870380](https://github.com/RDFLib/rdflib/discussions/1563#discussioncomment-1870380)>
[accessed 25 March 2022]
