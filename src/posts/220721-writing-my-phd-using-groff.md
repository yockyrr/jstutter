---
title: "Writing my PhD using groff"
permalink: false
date: 2022-07-21 16:50:00 +1
---

*In this blog post: thoughts on the tools available to a PhD researcher,
benchmarking Markdown to PDF using Pandoc, and thoughts on using groff for
academic work.*

I’ll begin with the obvious: a PhD is a difficult document to write. It is
similar in size and scope to a book, with the added constraint of monograph. For
the most part, you are effectively writing on your own. Academic publishing in
the twenty–first century, however, is becoming more collaborative. Perhaps it is
the kind of books that I am reading recently but I find that, more often than
not, an academic book published within the last decade is more likely to have an
editor, or set of contributing editors, than a single author. It is likely in
part also due to the fact that, in the postmodern, poststructuralist humanities,
readers are affronted by **any** book that communicates only one point of view.
Indeed, most disciplines have their examples of this.

# Twenty–first–century monograph man

As an example, a student of musicology is generally asked at some early point in
their academic career to write an essay comparing the single–author, *Oxford
History of Western Music* by the late Richard Taruskin — that characterises
Western music as a single narrative from the dark ages to the present — to the
multi-editor, multi-author series of the *Cambridge History of Western Music* —
which has a different editor for each volume, different author for each chapter,
and only the broadest sense of narrative possible. I have seen almost this exact
question on first–year undergraduate curricula at three different institutions.

It is a good question: the student is pointed towards the resulting discourse in
the academic literature, beginning with Taruskin’s review of the *Cambridge*
twentieth century history volume,[^1] and *Cambridge* editor Nicholas Cook’s
response,[^2] Harry White’s review of the *Oxford* history,[^3] and Taruskin’s
eventual defence of his position as music historian).[^4] If I may be allowed to
condense a far–reaching discussion into one sentence, the most common takeaway a
reader gets from all this is that Taruskin’s endeavour is laudable and
incredibly useful, especially for pedagogical purposes, but his history is
necessarily narrative and as a result biased to his personal views; the
differing and at times contradictory chapters in a *Cambridge* volume may offer
fresh insights and the idea that history does not have to be a story, or even
complete.

# Writing in the final year

For better or worse, a PhD is more like the former than the latter (and there
are of course arguments that can be made for how a PhD’s focus on monograph
excludes skill–based and collaborative research), yet it is generally required
that a successful PhD candidate will write a book–length study on the research
that they have completed over the last few years. The writing of that study — or
at the very least the conversion of draft material into final prose — usually
takes place in the final year of study, and this is where I am about to find
myself.

It would be easy for an outside observer to conclude that “writing up” usually
takes place mostly in the final year simply due to time constraints with
research and disorganisation on the part of the PhD researcher (and there are
without doubt many examples of rushed write–ups!) but what is more common is
that writing made in the first year of a PhD becomes essentially useless for the
final product. Very rarely does a PhD begin and end with the same goals in mind,
and in this way a PhD is a journey: you learn things and find things out during
research that alter your conception of a topic and therefore its research
consequences.

These changes may not be large on their own, but the accumulation of new
research questions, methodologies, and data can make your initial writing seem
wrongheaded. This is not to say that a PhD researcher should save all their
writing to the very end, rather the opposite: that they should do lots of
writing in order to flesh out and structure their thoughts, but not expect to
then be able to file that writing away and use it again. It is in fact the act
of writing and putting your thoughts into precise words **itself** that makes
you question your own preconceptions: ‘How should this be named/defined?’ ‘Why
do I believe this?’ ‘Does this argument stand up to scrutiny?’ With that said,
this blog post is not about those issues of monograph directly, but rather the
technical burdens that writing this way creates.

# Typical typesetting tools for a PhD researcher: Word, LaTeX

Feeding into the time pressure of the final–year write–up is that a supervisory
team are not editors, expert typesetters, figure makers, or publishers: nor
should they be expected to be. Granted, it is expected that a supervisor will
read what you write and give feedback on things, but it is ultimately up to the
PhD candidate to create the finished thesis. A supervisor gives advice but
should not hold your hand through the process. In fact, a supervisor that
insists on weekly meetings and continuous updates can be overbearing (in my
experience) and ruin your ability to think creatively. (These kinds of
supervisors are often to be found in situations where the success of the PhD
researcher is a tacit prerequisite to securing further funding for that
topic/supervisor/lab).

With respect to the thesis, historically the final product was made by passing
your manuscript to a typist who would create typewritten copy. However, since
the advent of computer typesetting, PhD candidates have taken this final burden
upon themselves. This has numerous advantages, not least the ability to pass
around copies more easily as digital files, receive comments directly on those
files, and go through multiple cycles of drafts without having to rewrite your
material by hand, as well as a stricter control of output. As a pretty
acceptable touch typist, I am at a personal advantage here. My advice however,
is still to write notes by hand as the pen–and–paper process better cements your
learning than skimming your fingers over a keyboard. I can better recall
something I have copied with my own hand than copy–pasted or typed.

## Humanities

However, with this new–found control comes the caveat that (most) PhD candidates
are very similar to their supervisors: they are not editors, expert typesetters,
figure makers, or publishers. As a result, most researchers in the humanities
instinctively reach for Microsoft Word or equivalent and begin by creating
“thesis.docx”. It is surprising to me how little thought often goes into the
organisation of a document’s internals with regard to styles and markup, and how
quickly draft prose starts appearing in a blank document. Perhaps it is due to a
lack of suitable computer literacy but, despite the tools being there, I see
only a minority of humanities researchers using basic style controls and
reference managers, and having to therefore laboriously go one–by–one through
their references and updating their footnotes, fonts and parenthetical citations
by hand. I do not mean that you should waste hours getting your fonts and
margins just right, but that many people seem to be unaware of the possibility
of using styles and reference managers that will in fact **save** them time in a
large and regularly–changing document.

This is perhaps exacerbated by the fact that most PhD researchers are broke,
using the same battered equipment they started their undergraduate with, and it
is not uncommon to see a 200–page, multi-image, multi-figure PhD thesis take
five minutes to open in Word on a five–year–old laptop, and then take twenty
seconds to scroll. Sometimes this effect can be ameliorated by starting each
chapter in a separate file (this is exactly how a previous supervisor sent me
their PhD thesis when I asked to read it), then merging the chapters together as
PDF.  However, this makes altering each chapter unwieldy, especially if you need
to have multiple chapters open at once simultaneously (for example, to move
content from one chapter to another), and the multiple instances of Word running
simultaneously can be far more taxing on the old laptop’s processor than one
larger document would ever be.

## Sciences

In the sciences, I hear that things are usually a little more organised. It is
at least common practice there to use a reference manager (unsurprisingly, and
as a result, reference manager systems are typically geared towards the needs of
science rather than humanities use). The more daring researcher may use LaTeX to
typeset their thesis. This may be their own install, but it is becoming more
common for institutions to provide LaTeX templates on an online platform such as
*Overleaf*. This is commonly thought of as TheRightWay™ to go about things, and
any LaTeX user will surely love to tell you about its superior typesetting
(kerning, ligatures etc.) and how its text–based format even allows them to
integrate their document as a git repository.

I know this because up until a few months ago I was that person. Who knows how,
but I came to love LaTeX as a teenager, and carried my beautifully–typeset
essays all through my music undergraduate (although their beauty stops at their
aesthetic). I wrote my Master’s thesis in LaTeX and marvelled at how Word would
not have been able to cope with the hundreds of pages of appendices I required.
It looked great and I felt great. That is, until I came across groff. Now I’m
trying to pare my documents down to the minimum requirements possible in order
to give myself greater room to write.

# Groff in Pandoc

Perhaps surprisingly, my first true clash with groff was with Pandoc.[^5] As a
long–time Linux user, I had always heard the name of groff and its \*roff
cousins being used in the context of manpages (terminal–based documentation
pages), and in the world of LaTeX many tools work both with groff and TeX. Groff
is part of an ancient hereditary line of typesetting systems stretching back
nearly 60 years. Groff (first released 1990) is the GPL–licensed version of
troff (mid 1970s), which was a typesetter–friendly version of roff (1969),
itself a portable version of RUNOFF (1964) for the IBM 7094. My initial
impression, then, of groff was of a dinosaur, better–suited to terminal and
ASCII output than creating modern PDFs. I dismissed groff out of hand as a
rudimentary and more obtuse predecessor to LaTeX.

It was during the writing of one of my earlier blog posts (in Markdown) that I
decided that I needed a nicely–typeset PDF copy to read in full before editing
and posting. I do not find it ideal to read blog posts directly in Vim, or even
copying the code into Word. For me, I can read better (and be less distracted)
when something is written as if it were on A4 paper or a physical book. I
resorted, then, to Pandoc to convert my Markdown code into a nice, readable PDF.
This is as simple as invoking `pandoc -s -o post.pdf post.md` and by default it
uses pdflatex as the backend to create the resulting PDF file. However, as I was
searching for margin options in the documentation, I came across the command
line option `--pdf-engine=PROGRAM` which allows Pandoc to switch to another
means of generating PDF.[^6] There are eleven options:

- pdflatex
- lualatex
- xelatex
- latexmk
- tectonic
- context
- wkhtmltopdf
- weasyprint
- pagedjs-cli
- prince
- pdfroff

These can be split into three broad methods:

1. Six LaTeX methods using four engines (pdflatex, lualatex, xelatex, context),
   latexmk being a frontend to pdflatex and tectonic a frontend to xelatex
2. Four HTML to PDF converters requiring a two–step process from Markdown to
   HTML then to PDF
3. pdfroff: on my system this is groff with the ms macro set

Let’s look at their outputs (p.2):

## pdflatex

<figure><img src="images/220721/blogpost-pdflatex.png"
alt="pdflatex"/></figure>

## lualatex

<figure><img src="images/220721/blogpost-lualatex.png"
alt="lualatex"/></figure>

## xelatex

<figure><img src="images/220721/blogpost-xelatex.png"
alt="xelatex"/></figure>

## latexmk

<figure><img src="images/220721/blogpost-latexmk.png"
alt="latexmk"/></figure>

## tectonic

<figure><img src="images/220721/blogpost-tectonic.png"
alt="tectonic"/></figure>

## context

<figure><img src="images/220721/blogpost-context.png"
alt="context"/></figure>

## wkhtmltopdf

<figure><img src="images/220721/blogpost-wkhtmltopdf.png"
alt="wkhtmltopdf"/></figure>

## weasyprint

<figure><img src="images/220721/blogpost-weasyprint.png"
alt="weasyprint"/></figure>

### pagedjs-cli

<figure><img src="images/220721/blogpost-pagedjs-cli.png"
alt="pagedjs-cli"/></figure>

## prince

<figure><img src="images/220721/blogpost-prince.png"
alt="prince"/></figure>

## pdfroff

<figure><img src="images/220721/blogpost-pdfroff.png"
alt="pdfroff"/></figure>

These outputs all look fine for most uses. Obviously, the LaTeX engines look
very good and the HTML engines are of varying quality (wkhtmltopdf is the best
of these in my opinion, the freemium prince is high quality but leaves a
watermark on the first page, weasyprint is okay, and pagedjs-cli was not very
acceptable given that it added extra blank pages and spacing as well as ignoring
my margin parameters). However, what surprised me was the output of groff which
for something I had only seen the output of manpages, produced quite a
professional–looking PDF.

# Speedy groff

Quality was not the only way in which groff surprised me: it has serious speed
too, especially in comparison to LaTeX. Let’s benchmark these PDF engines on my
most recent blog post (100 runs, images removed):

<figure><img src="images/220721/pdfengineruntimes.png" alt="Graph of runtimes of
PDF engines"/><figcaption>Benchmarked pandoc PDF engines (lower is
better)</figcaption></figure>

The LaTeX engines are extremely slow, and this came as no surprise to me. At the
end of writing my Master’s thesis, I was often waiting over a minute for XeLaTeX
to compile my document with its multiple figures and musical examples. It may
look extremely beautiful, but it takes a long time to iterate the feedback (or
“edit–compile”) loop. This is the same with programming: if your compiler is
slow then it usually does not matter how optimised its output is, the slow
compiler is wasting valuable programmer cycles rather than cheap CPU cycles. I
believe also that a slow feedback loop hinders flow state, as every time you’re
waiting for the program/document to compile, you lose a little bit of
concentration. It is for this same reason that I cannot stand doing web design
using a framework where I cannot see the results of my changes almost instantly.
If I have to wait more than five seconds to see the results of my slight change
in padding, then I become frustrated with the process.

The HTML engines are, once again, of varying speed. Prince is by far the
fastest, but groff comes in a close second, with a much better quality. Groff is
notable, too, for its output size:

<figure><img src="images/220721/pdfenginesizes.png" alt="Graph of sizes of
resulting PDF files"/><figcaption>Pandoc PDF engine output sizes (lower is
better)</figcaption></figure>

I believe this is largely due to the fact that groff in my case embedded only
four fonts in its output PDF, whereas the LaTeX engines embedded up to twelve. I
would think therefore that the size advantage for groff would diminish on
longer, larger files. For me, the Markdown engine of choice is pdfroff.

# Greater things

Using groff as my preferred backend for converting Markdown to PDF piqued my
interest in the system, and soon I found the macro set called “Mom” that applies
numerous quality–of–life changes and sensible defaults to groff. Mom has a good
set of documentation,[^7] and it didn’t take me long to begin writing documents
using the macro set. I had no idea that groff could accomplish
citations/referencing (using *refer*), figures (by including PDF images created
using Imagemagick), diagrams (using *pic*), tables (using *tbl*), graphs (using
*grap* — those benchmark graphs above were created using grap), tables of
contents, cover pages! Not only is groff quick to compile, but also quick to
write. Most commands are a period and a few letters on a new line, rather than
the complicated brace expressions of LaTeX.

I recently updated my CV (in LaTeX) and the process underscored how much of my
typesetting was just “try something and see how it looks”. Not only did this
take a long time to refresh using LaTeX, but no matter how I indented the file,
I could not make the markup look clean. I realise now how much I am fighting
LaTeX whenever I stray from the happy path (that path being almost unilaterally
text and equations).

In groff, the typically all–caps commands stick out to me and, just like LaTeX,
are generally semantic rather than aesthetic (“create a heading” rather than
“create large bold text”). Although I have never used WordPerfect, I am reminded
of how people speak of its “Reveal codes” mode in hushed tones. This quick
writing works well with my preferred workflow of vim + terminal. I write a
Makefile with a “watch” rule that calls Mom’s frontend `pdfmom` in a loop with
`inotifywait`. As soon as I save a file in that directory, the PDF is compiled.

In fact, many of the advantages of using groff are the same as LaTeX, but with
the added advantage of speed and simplicity. Just like with LaTeX, you can dive
into the code to change a macro but, unlike LaTeX, I have found Mom’s macros
fairly understandable and not nested ten layers deep in some ancient package.

# Notes on using groff for academic work

I contain my files within a git repository and make use of all the advantages
contained therein, particularly stashing changes for later and working on new
material in a separate branch.

I use Mom’s `.COLLATE` macro to keep chapters in separate files, and maintain a
“thesis.order” file that describes which files are to be concatenated in what
order. The Makefile reads this file and simply `cat`’s the files together,
piping into `pdfmom` which handles the preprocessing with refer, tbl, grap and
pic.

The manpages for the preprocessors are eminently readable, with a strong focus
on tutorial rather than simply documentation of “what you should already know”,
but I would also recommend:

- Mom’s own documentaton page on refer.[^8]
- James and Schaffter’s PDF document, especially for creating internal links.[^9]
- Bentley and Kernighan’s paper on grap (warning: Postscript).[^10]
- Raymond — Making pictures with GNU pic.[^11]

Below a sample of the in–progress PhD. I don’t want laggy Word and inconsistent
styling issues. I don’t want KOMA-Script woes and minutes–long compile times. I
want something simple that works: groff!

<figure><img src="images/220721/thesis-sample.png" alt="Sample page from the
thesis"/><figcaption>Sample page from groff–powered
thesis</figcaption></figure>

[^1]: Richard Taruskin, ‘Review: Speed Bumps’, *19th–Century Music* vol.29, no.2
(2005), pp.185–295
<[https://doi.org/10.1525/ncm.2005.29.2.185](https://doi.org/10.1525/ncm.2005.29.2.185)>
[accessed 21 July 2022]

[^2]: Nicholas Cook, ‘Alternative Realities: A Reply to Richard Taruskin’,
*19th–Century Music* vol.30, no.2 (2006), pp.205–208
<[https://doi.org/10.1525/ncm.2006.30.2.205](https://doi.org/10.1525/ncm.2006.30.2.205)>
[accessed 21 July 2022]

[^3]: Harry White, ‘The Rules of Engagement: Richard Taruskin and the History of
Western Music’, *Journal of the Society for Musicology in Ireland*, vol.2
(2006), pp.21–49
<[https://www.musicologyireland.com/jsmi/index.php/journal/article/download/12/11/43](https://www.musicologyireland.com/jsmi/index.php/journal/article/download/12/11/43)>
[accessed 21 July 2022]

[^4]: Richard Taruskin, ‘Agents and Causes and Ends, Oh My’, *Journal of
Musicology*, vol.31, no.2 (2014), pp.272–293
<[https://doi.org/10.1525/jm.2014.31.2.272](https://doi.org/10.1525/jm.2014.31.2.272)>
[accessed 21 July 2022]

[^5]: John MacFarlane, ‘About pandoc’, *Pandoc*
<[https://pandoc.org](https://pandoc.org)> [accessed 21 July 2022]

[^6]: ‘Creating a PDF’, *Pandoc*
<[https://pandoc.org/MANUAL.html#creating-a-pdf](https://pandoc.org/MANUAL.html#creating-a-pdf)>
[accessed 21 July 2022]

[^7]: Peter Schaffter, ‘Documentation’, *MOM – Macros for GNU troff*
<[https://www.schaffter.ca/mom/mom-04.html](https://www.schaffter.ca/mom/mom-04.html)>
[accessed 21 July 2022]

[^8]: Peter Schaffter, ‘Bibliographies and references’, *Mom*
<[https://www.schaffter.ca/mom/momdoc/refer.html#top](https://www.schaffter.ca/mom/momdoc/refer.html#top)>
[accessed 21 July 2022]

[^9]: Deri James and Peter Schaffter, ‘Producing PDFs with groff and mom’ (2012)
<[https://www.schaffter.ca/mom/pdf/mom-pdf.pdf](https://www.schaffter.ca/mom/pdf/mom-pdf.pdf)>
[accessed 21 July 2022]

[^10]: Jon L. Bentley and Brian W. Kernighan, ‘Grap — A Language for Typesetting
Graphs Tutorial and User Manual’ (1991)
<[http://www.kohala.com/start/troff/cstr114.ps](http://www.kohala.com/start/troff/cstr114.ps)>
[accessed 21 July 2022]

[^11]: Eric S. Raymond, ‘Making pictures with GNU PIC’ (1995)
<[https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.136.8343](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.136.8343)>
[accessed 21 July 2022]
