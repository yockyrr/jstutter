---
title: "Back of the Envelope"
permalink: false
date: 2021-02-12 13:05:00 +0
---

To cut a long story short, I had **vastly** miscalculated the costs for using
the :sparkles: magic :sparkles: of Google, and they were going to send me a bill
for £200. Panic, delete account, please don’t charge me, I can’t afford that.
Well, that was a bit of a dead end, but I’m a lot wiser now (see last post). The
“RightWay” can be a productive way of working, if you have a budget and a whole
team of people to support. I have neither, so I returned to the world of the
pragmatic, and spent a few days migrating everything off Google and to *Mythic
Beasts*, an independent provider based in Cambridge with a fantastic name, which
I have used before.[^1] They charge me a few quid for the pleasure, which is a
small expense I am happy to deal with! Nothing is as magic or as new, my efforts
to automate everything have been practically for nought, and I will have to do
the ML myself on my own computer, but at least it works.

# Better ML

CANDR now has a temporary domain name: www.candr.tk, where I have begun to input
some of W2 to test the ML.[^2] More tweaking and cleaning up of the ML code has
improved the accuracy a surprising amount. The boring detail is to do with
weighting. Take, for example, the notes. I preprocess the data by (basically)
making two images. Image one (the input) is the original stave, and image two
(the output) is blank with the features I wish to detect marked up (it is a
little more complex than this, I’m not using pretty colours). Put simply, the
training phase of the ML takes both images and attempts to alter its neural
network to best recreate the output from the input.

<figure><img src="images/210216/stave-27.jpg" alt="The original stave against a pretty representation of the output"/><figcaption>The original stave against a pretty representation of the output</figcaption></figure>

As you can see from the image, in this case there are many more times blank
“nothing” spaces than useful features, so the ML frequently got away with just
predicting nothing for every sample, the important notes and items would not be
enough to get it off the ground, so to speak. To counteract this, during my
preprocessing, I now count the number of “nothings” and the number of features.
Say I have 100x more nothing than feature, I can then pass the inverse of that
into the training, such that if the ML predicts a note wrong, it is 100x more
likely to effect a change on the neural network than predicting a blank space
wrong.

# RAM Emergency

<figure><img src="images/210216/RAM-emergency.jpg" alt="Yeah, there was a RAM emergency, the office had too much ... RAM?"/><figcaption>I wish. Everyone remembers this episode of *The IT Crowd* for the “street countdown” storyline, but forget Jen’s exclusion from Douglas’ secret workout sessions. A reminder that the writer of *The IT Crowd*, *Father Ted* and *Black Books* is rightfully cancelled and permabanned from Twitter for being transphobic.</figcaption></figure>

Another problem that I have run into is the ever–increasing size of my dataset.
My computer has a fair amount of RAM, and the dataset fits easily in that space
(currently sitting at around 2GB after preprocessing), however the RAM issue
lies in the implementation and what should hopefully be the last of the gory
technical detail. The language I am using, Python, is the most popular and
supported language for developing high–level ML in, as it is simple and fast to
develop in and easy to pick up with few gotchas. However, it gets itself in
trouble with extremely large datasets.

Python, like most modern programming languages, is *garbage collected*, in that
every time you make a new variable, it automatically allocates space for that
variable but crucially you do not have to tell it when you’ve finished with that
memory or manually delete everything as you go along. Every so often, the
garbage collector finds all the variables you’re no longer using, and frees that
memory back to the operating system. AFAIK, Python’s garbage collector is quite
lazy: it doesn’t come round very often and will regularly not actually take the
garbage away, thinking that you actually haven’t finished with it, and you’re
intending to use it later. Often this is correct, and for small programs it
doesn’t matter very much as all the memory is automatically freed when the
program finishes. However, for long–running programs (like this one!) we can
quickly run out of memory, compounded by another issue called memory
fragmentation which I won’t go into.

Under the hood, Python uses the allocator `malloc` to get memory from the
operating system. `malloc` takes a single argument: how much memory you would
like allocated. `malloc` does not warn the garbage collector that we’re running
out of memory, or force it to return memory to the operating system. `malloc`
often will keep allocating memory until something breaks. I believe this was
what was occurring with my program. Although the dataset was only 2GB and I have
many times that available on my computer, my program was passing through the
data multiple times, allocating different copies of the data as it went,
fragmenting and duplicating until it ran out of memory. I managed to mitigate
this somewhat by manually marking large variable as done with using Python’s
`del`, but often they still would not be garbage collected (`del` is a *marker*
for deletion, not a command to delete-right-this-very-second). This problem was
only going to get a thousand times worse when I increase the granularity of the
ML, and use more staves as training data. Even if I managed to completely quash
the memory fragmentation issue, I anticipate that my dataset will grow large
enough not to fit in RAM, even in the best of cases.

I therefore must save my dataset to disk, and access it a bit at a time when
training. To do this, instead of saving all the data into one huge variable, I
developed a small `DataFrame` class (inspired by a popular data science library
base class of the same name) that wraps a SQLite database. Instead of keeping
the data in memory, before training it is packaged up into batches and saved
into the database file. This also has a side effect advantage of forcing the
memory to be reorganised contiguously. Each training batch fetches only the next
record from the database rather than the entire dataset, and so the size of the
dataset should now be limited only by the size of my harddisk. Disk is slow
however, many times slower than RAM and it takes upwards of half an hour of
crunching to package the data and save it to disk. I really don’t fancy fine
tuning it any further, so I now use that dataset as a cache that can be fed back
into the training rather than recalculated on each and every run.

Phew! Hopefully that should be the final technical tangle I get myself in.

# Putting it all together

There are still some silly little bugs plaguing the transcription interface, the
most most notable of which being *divisiones* that rudely do not delete properly
when asked, sudden bouts of unresponsiveness, and *plicae* being applied to the
wrong notes. However, I can work around these, and most importantly I have added
a lovely big button to fetch the ML predictions from the database and populate
the stave for checking and fixing. Please enjoy this video of me transcribing
some staves with the help of ML.

<figure><video width="1920" height="1080" controls preload><source src="images/210216/candr-demo.mp4" type="video/mp4"></video><figcaption>I ramble and transcribe a system.</figcaption></figure>

So how long is this going to take, and is it feasible? At the database prompt,
here are two useful commands to give me an idea (comments on the right hand
side).

<figure><img src="images/210216/sql-timing.png" alt="SELECT COUNT(*) FROM folios WHERE folios.deleted_at IS NULL; SELECT AVG(idcount) AS average FROM (SELECT COUNT(staves.id) AS idcount FROM staves INNER JOIN systems ON staves.system_id = systems.id INNER JOIN facsimiles ON systems.transcribe_id = facsimiles.id INNER JOIN folios ON facsimiles.folio_id = folios.id WHERE staves.deleted_at IS NULL AND systems.deleted_at IS NULL AND facsimiles.deleted_at IS NULL AND folios.deleted_at IS NULL GROUP BY folios.id) AS subq;"/><figcaption>Two database commands to calculate my heuristic.</figcaption></figure>

The first command reveals that there are 1784 folios in the database. My
terminology is a little confusing as in the database, folios are defined as
manuscript surfaces that are named (n.b. *named* rather than numbered). For
example, “42r” is a different folio to “42v”, even though we would call them the
same folio as it has a different name. In practice, folio is better approximated
as page rather than folio as one “folio” usually has a single facsimile. This
figure of 1784 includes all the facsimile images of F, W1 and W2.

The second command indicates that, of the currently–defined staves, there are on
average 8.83 staves per folio. This means 1784 x 8.83 = 15752 staves to
transcribe in total. I have transcribed 273 of them in the past week with and
without the help of ML. Without ML it takes a gruelling eight minutes per stave
to get everything sorted, the majority of the time being taken clicking on all
the notes. With ML at its current accuracy, the time is taken down to between
two to three minutes per stave. The most successful predictions are staff lines
(almost perfect each time) and notes (mostly perfect). This is nearly a
threefold improvement and goes to show that this has not all been for nothing!

But can I do it all? Time for a back–of–the–envelope calculation. Here’s the
envelope:

<figure><img src="images/210216/back-of-the-envelope.jpg" alt="1784 folios x 8.83 staves per folio x 3 mins per stave = 47258 mins ÷ 60 = 787 hours ÷ 8 hour day = 98 days = 19 weeks ≈ 5 months?"/><figcaption>This blog post was delayed because I couldn’t find an envelope for this bit: one of the downsides of going paperless</figcaption></figure>

So, about five months of forty–hour weeks just sitting at the computer pointing
and clicking? This is such an interesting figure. It’s neither so small as to be
a simple endeavour, like a fortnight, nor so large as to be an unreasonable
task, like a year. Likely it would take a little longer due to various issues
that will surely crop up, but then again I anticipate that the ML will improve,
not by too much as to completely change matters, but small improvements. I think
five months is a good estimate.

I could say that it is all too much and I should trim things down, but on the
hand the idea of having it *all* is so inescapably alluring. To be honest I am
not sure, with COVID as it is, that I’ll be doing much else this summer, and
will probably be stuck inside again anyway (sorry for the downer). Perhaps five
months of clicking will be the best use of my time‥?

[^1]: *Mythic Beasts* <[https://www.mythic-beasts.com](https://www.mythic-beasts.com)> [accessed 5 February 2021]
[^2]: Joshua Stutter, *CANDR* <[https://www.candr.tk](https://www.candr.tk)> [accessed 5 February 2021]
