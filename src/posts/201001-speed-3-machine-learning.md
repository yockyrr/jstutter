---
title: "Speed 3: Machine Learning"
permalink: false
date: 2020-10-01 18:24:00 +1
---

The git repositories that host CANDR are split into two: `yockyrr/candr` and
`yockyrr/candr-data`.[^1] The former hosts the codebase and scripts for CANDR to
operate, and the latters stores a dump of the database in a text format (a raw
SQL dump) along with the images uploaded to the database as well as the
necessary scripts used to transform to and from the database format (SQLite).
The project clones both of these repositories and keeps them in separate
directories, such that a change in the database does not effect a change in the
codebase, and vice versa.

# Manual tracing

I’ve been fixing bugs in the frontend of CANDR by uploading sets of facsimiles
and attempting to input a few staves here and there, seeing what issues crop up
along the way. Exactly as I had hoped, input is relatively fast, such that I can
trace the key features of a stave of ND music and save it to the database for
processing in under a minute. I am aiming to get a large proportion of the ND
repertory traced in this manner to create a large dataset for analysis by next
year. I have implemented an automatic guide on the site (the “Transcription
Wizard”) that tracks transcription process and automatically leads you through
each step of the tracing and transcription process for an entire source of
music. The wizard tool takes you through the categorisation of a source to its
stave transcription, and each stave is quite simple to transcribe.

#[Labelled screenshot of stave editor window: 1. Toggle facsimile visibility 2. Toggle staffline visibility and below, add staffline 3. Clef visibility and add 4. Accidental visibility and add 5. Divisione visibility and add 6. Note visibility and add 7. Syllable visibility and add 8. Tools for adding ligatures and adding notes to ligatures 9. Multi-input tool. When toggled, as soon as an element has been added, automatically adds another of the same type 10. Delete selected element 11. Various tools for adding editorial elements](images/201001/labelled-editor.png)

Using this editor, my personal workflow is thus:

1. Trace the stafflines using the staffline tool (2). Use the multi-input
function (9) to trace all of these at once. There are typically four to six. It
is important to trace these first as they will form the basis for the pitch
content of other elements such as clefs, accidentals and notes.
2. Trace non-note data. I usually like to begin with the clef (3) as all staves need
one of these (existing or editorial), then a starting accidental if present (4),
finally using the multi-input tool once again to trace the divisiones (5).
3. Trace the notes (6). Use the multi-input tool to click on the note heads,
taking care to see which have plicae and which will be transcribed incorrectly
by looking at the green staffline outlines.
4. Go back over the notes and add plicae and manual shifts where necessary to
correct the notes’ transcription.
5. Add syllables to notes (if present) making a best guess as to which note this
syllable is attached to.
6. Finally, use the transcription wizard (not in screenshot) to mark this stave
as transcribed.

Once all staves in a system have been traced, you can then use the
transcription wizard to mark the system as transcribed, and it will suggest
that you add system synchronisation tags to the system. This is a fancy way of
saying: add lines between staves that indicate that these events occur
simultaneously. A setting of music can then be broken down into “chunks” of
polyphony for processing in XML (see previous posts). It will be my aim during
analysis to detect which chunks can be grouped together to form clausulae and
then how these clausulae are passed around the repertory.

A chunk can be as small or large as you like, but I like to make as little
editorial input with regard to inferred rhythm as possible, relying on visual
cues to synchronise the staves, such as alignment and the size and shape of
divisiones on the page. The scribes of the manuscripts clearly intend some
moments to be synchronisation points. For example, it may be possible to imply
through rhythmic interpretation that two notes occur simultaneously, but it is
more obvious that two divisiones are simultaneous when they are written
directly above one another at the end of a system. In this way, it is usually
short, synchronous ordines of polyphony that are collected into chunks. Longer,
more hocketed passages I will usually shy away from making into smaller chunks
as I don’t wish to editorialise a rhythm upon polyphony that is not fully
understood. If such hocketed passages are to be passed around in clausulae,
then they are likely to be transmitted as single units: half a hocket doesn’t
make musical or poetic sense.

# Uploading the dataset with some script-fu

In August, I grew bored of just drawing some lines and boxes so decided to
source the public domain or permissibly–licensed manuscript images for the
three main sources (F, W1 and W2) and upload them in total to the website. The
images of these can be downloaded as a large set of numbered JPEGs from the
respective library websites, so I had a task at hand to organise these JPEGs
and label them, then upload them one–by–one to the website, creating a folio
and facsimile record for each, then grouping them all into facsimile sets. As
can be seen from the folder of ID-named files in the image store of the data
repository, this was a lot of files to upload![^2] However, I did not complete
this mammoth and repetitive task alone. I wrote two shell scripts
(`foliator.sh` and `uploader.sh`) to streamline the tagging and uploading
process of these files.[^3]

The JPEGs as I had downloaded them were luckily saved in a numbered order, so
moving back and forth through the numbers in an image viewer moved back and
forth in the manuscript. By flicking through the manuscripts, I took a note of
where the foliation was broken, repeated, or needed editorialising.
`foliator.sh` simply prompts for four numbers:

1. The filename of the first JPEG.
2. The folio that this JPEG is a facsimile of (recto was assumed, and the first
images of JPEG sets that began on verso were manually inserted).
3. The final folio in this set.
4. The “width” of the filename, e.g. a width of five would be numbered
00001.jpg, 00002.jpg, 00003.jpg etc.

Finally, it asked for the filname extension (typically .jpg) and generated a
TSV (tab separated value) file which linked folio name to a calculated filename
for this folio. The script creates two records: one for recto and another for
verso. Broken foliation could be calculated by doing one pass for each
contiguous set then appending the files in `vim`, and filenames that had
prefixes were fixed using a mixture of `sed` and `vim`. After manual review,
this script gave me a file that contained folio labels and a matching JPEG for
each.

The second script, `uploader.sh`, reads in this generated TSV file and parses
it. After inputting user credentials for the CANDR website instance,
`uploader.sh` uses the site’s API to log in, create correctly–named folios and
facsimiles for each record, and uploads the image to the site’s database. Where
each image previously took a few minutes to tag and upload manually, whole
facsimile sets were uploaded in a matter of seconds. As of `yockyrr/candr-data`
commit `c74fac5a`, all of the three main sources have been uploaded.[^4]

# A startling realisation

Looking at all these facsimiles uploaded to the website was quite awe–inspiring.
These are all the images that I want to transcribe and it was actually rather
worrying to scroll through the images and think, “I have to transcribe all of
these”, then look at the little progress metric I added at the top which
reminded me that I had so far to go.

<figure><video autoplay preload loop muted poster src="images/201001/scrolling.mp4"></video><figcaption>Cue soundtrack from <i>Interstellar</i></figcaption></figure>

When travelling, I like to continuously work out in my head how much farther it
is to go and how long it will take me to get there, especially on long boring
journeys where I know I will be travelling at a constant speed. I enjoy
consoling myself with a calculation, e.g. if I’m driving at 60mph, that’s one
mile every minute, so if home is 46 miles away then it’s going to take me about
46 minutes to get there. Then I like to compare how accurate I was when I
eventually arrive. Similary when I find it difficult to sleep, I likely make it
worse for myself by looking at the clock and thinking, “If I fall asleep *right
this very second*, I’ll sleep for exactly 4 hours and 24 minutes before my alarm
goes off” (n.b. this rarely results in me falling asleep any faster).

It came as no surprise to myself then that when looking at all the images I had
to transcribe, I did a quick back–of–the–envelope calculation to see how long
all this is going to take. This time two years ago, I was perhaps naively
setting off on transcribing a large proportion of W1 for my Masters, the
transcription of which took far longer than I anticipated (roughly three months
off and on) and I don’t think I’ve ever been the same. Moreover, I wasn’t
trying to capture as much information then as I am doing now, and input was
likely just as fast, if not faster.

I timed myself inputting a few staves, added a little on for redos and general
faff, multiplied by a ballpark figure for the number of staves per facsimile
and the number of facsimiles in the database and arrived at an alarming figure.
I won’t share that figure here, but suffice to say that at 40 hours of
transcription a week, I’d be lucky to finish transcribing my dataset by next
Christmas. No, not Christmas 2020, Christmas **2021**. Who knows what the world
will look like by then? I shudder to think about that, as well as my mental
state after such an undertaking.

# Search for help 1: Image segmentation

If there has been any work done on using digital technologies to increase the
speed of music transcription, then the work done over nearly two decades by the
DDMAL team, mostly based at McGill, on OMR (optical music recognition) would
likely have researched this.[^5][^6] By using OMR, researchers have managed to
extract meaningful categorisations of music by developing numerous in–house
tools, the most interesting for this purpose listed on their website being
Gamera.[^7] Gamera styles itself as not simply a tool for OMR, but a tool for
generating OMR tools, a sort of meta-tool that has had equal success in text and
music. After dutifully downloading and installing Gamera, I set to opening a
facsimile of music and attempting to extract some meaningful features from it.

#[Gamera’s best go at ND repertory](images/201001/gameras-go.png)

Unfortunately, the feature extraction filters of Gamera didn’t perform well
here. The main issue with detecting staff notation is that elements are layered
on top of one another: elements such as clefs, accidentals and notes are layered
on top of the stafflines, and in the ND repertory notes are joined into
ligatures of no fixed shape or size. Unlike text, where the individual glyphs
that make up words can be extracted individually and recombined to make words,
staff notation requires a larger context to understand elements. For example,
whether a horizontal line is a staffline or ledger line requires looking further
afield to the edges of the stave to see whether that line continues. This makes
it difficult or impossible for filter–based feature extractions as used in
Gamera to extract features that rely on larger contexts. To attempt to remove
stafflines from common practice printed music, Gamera needs a special toolkit
which does not work on ND manuscripts.[^8]

# Search for help 2: Machine learning

However, DDMAL have more recently begun work that uses machine learning (ML) to
classify pixels of early music facsimiles, for example. Pixel.js builds upon
Diva.js (an IIIF viewer), and is a web–based editor that allows users to
categorise the pixels of a manuscript: notes, ligatures, stafflines, text,
etc.[^9] What they call the “ground truth” of these pixel classifications can
then be fed into an ML algorithm (DDMAL uses a scheduler called Rodan) which can
then be trained on that input data. Then, when asked to predict the
classification of an unknown pixel from another manuscript, it can attempt to
predict what class of item that pixel belongs to. Of course, it is impossible to
guess a pixel from that pixel alone, so a window of image context is given to
the training data (typically 25 pixels square). The ML model is therefore asked
a simple question: given this 25x25 image, what is the class of the central
pixel?

<figure><video autoplay preload loop muted poster src="images/201001/pixel-js-is-laborious.mp4"></video><figcaption>Transcribing facsimiles with Pixel.js is incredibly laborious and difficult when you need pixel–perfect accuracy. It is easy however, to ragequit.</figcaption></figure>

All this sounds great, until you realise the timescales involved for generating
*any* ground truth data. DDMAL proudly state that using Pixel.js, one can
generate ground truth for a single facsimile of music in just eighteen
hours![^10] This approach clearly won’t work in my case as I do not have the
time to generate pixel–perfect ground truth data. Furthermore, I am a little
sceptical of DDMAL’s approach, and their narrow focus on classifying pixels
rather than extracting features. Consider a best–case scenario: the ML model has
been exceptionally well trained and is asked to predict the classifications of a
facsimile that is 1000x1000 pixels. Say the model is 99% effective, and you are
then interested in extracting stafflines. Even at 99% accuracy (an unreal
standard), the staffline layer will not consist of neat stafflines, but a series
of correctly–predicted segments and incorrectly–predicted anomalies, which will
then have to undergo further processing to be smoothed out into “real”
stafflines.

Remember: a staffline or other element isn’t a tangible item, but a notational
abstraction based on a collection of similarly–functioning notational elements.
That’s why I put “real” in big scare quotes here. When considering what a “real”
staffline is, aesthetic is everything and a staffline is only as good as its
visual manifestation. There is a tempting avenue in considering these old
manuscripts as inferior technology and making the case that if only we could
just get it to display in \[notation editor of choice\] then we would be okay.
There is something unsettling about this, a feeling like we’re colonising the
past somewhat and I believe it’s important to continuously remind ourselves that
our notational representations are just as valid as the originals. We’re losing
and gaining information during these transformations.

This focus on the pixel also ignores the context of the music. DDMAL correctly
acknowledge that there are layers to the facsimile (stafflines, notes, text,
etc.) but fail to mention that these layers *overlap*. When a note is on a
staffline, the staffline is still implied to lie behind, hidden by the note
which has been written on top. A pixel, or matrix of pixels, is not a perfect
representation of the context of the underlying music nor the notation on the
page, and a classification of a pixel as one thing or another fails when a pixel
could in fact be two or more things at once. 

This is not to say that DDMAL’s method is not effective: they have seen success
in classifying pixels this way, and I have not seen much output from their
models except a few cherry–picked examples. I would contend that we need a
greater diversity in approach in order to detect diverse features in diverse
contexts. However, it is one thing to say it and another to actually go and *do*
it.

# Roll your own

Rather than a single ML model that classifies all pixels as one of a single
class, I decided to create individual ML models for detecting particular
features, extracting features directly rather than classifying pixels. This has
the advantage that I can tailor my approach to the feature I am trying to
detect. I decided to start by detecting stafflines, as they are the most
time–consuming and repetitive elements to add, and I figured (perhaps
incorrectly) that they would be simple to detect.

The most well–used and popular library for ML is TensorFlow.[^11] In the past it
was difficult for novices to get started using the sometimes confusing libraries
such as TensorFlow, but recently simple APIs such as Keras have made that much
simpler, by hiding some of the unnecessary configurations and providing sensible
defaults that can be tweaked later.[^12] ML has had such a high barrier to entry
for this very reason, plus the fact that every option and algorithm used has a
complicated name derived from a research paper. In reality, most of the things
that you want to do with ML have already been done, and it is just a matter of
finding the right blog post or StackOverflow question that works for you.

# Asking the right questions

The most important step is trying to simplify the question you’re asking as much
as possible, and trying to get as much data for that question as possible.
Usually these go hand–in–hand: if you’re simplifying your question, you’re
likely to be splitting up your data into smaller and smaller chunks. For example
in this case, the question I’m looking to answer is: looking at a picture of a
stave, where are the stafflines? I want to better define that question and break
the problem down into smaller sub-problems to solve individually.

First of all, it is important to think about how the ML model “sees” the data.
It’s not looking at an image or sequence and making classifications. What it is
in fact doing is relating two matrices of numbers, and trying to guess the
second from the first. For example, the 25x25 window that DDMAL use is usually a
colour image. There are three components to a colour image: red, green and blue.
Say I’m only trying to find stafflines, the output is not a simple
classification but a probability: staffline or no staffline. Therefore, the
input is a matrix with shape `25, 25, 3` (i.e. width 25, height 25, depth 3 for
the 3 colour channels) and the output is a matrix of `2, 1` (width 2, height 1)
to make two probabilities for each of the classifications. The ML model doesn’t
know that the input matrix is an image and the output is a probability, it’s
just numbers that it’s trying to guess using the input numbers.

For instance, a naive interpretation would be to take your colour image, feed it
directly to the ML model and extract probabilities from each pixel. Say your
image is 1000x1000, then your input shape is `1000, 1000, 3` and the output is
`1000, 1000, 1`. This is not going to yield a good model as you’re expecting too
much from the model, and your training data is going to be only in the dozens
(one per stave). This is quite a few orders of magnitude off as we want at least
*thousands* of samples for training.

Better are approaches that simplify the question, such as DDMAL’s 25x25 pixel
window and the classification of a single pixel. I want to go down the same
route, but simplify my questions even further. For the problem of stafflines, I
see it being broken down like so:

1. How many stafflines are there? (Staves commonly have between four and six).
2. Which areas have stafflines in? (This is known as region of interest or ROI
for short).
3. Where in these areas are the stafflines?

The difference between DDMAL’s and my approach is that rather than asking the
pixel location of stafflines in an entire stave, I can first narrow my search by
finding general staffline parameters and areas, then doing a fine–grained
location search in those areas.

# The ML process

First: to define exactly the data I will be working on. Calling back to previous
posts, CANDR applies perspective transformations on facsimiles to isolate
systems and then applies another round of transformations to isolate staves
(perhaps there is another ML application here), so in this case I first need to
preprocess my data to gain an initial dataset of the perspective–transformed
staves, as seen by the CANDR stave editor. This is all done in Javascript in
browser, and so to get that same data for the ML models, I had to port my
Javascript code exactly to Python and verify that viewing a stave in Javascript
is the same output as viewing a stave in Python (n.b. this is the third time
I’ve rewritten the same functions in a new language: I previously had to port
the same Javascript code to PHP for the backend).

There are (broadly) three steps to ML:

1. Training. Give the model a whole load of **input → output** cases and let the
algorithm work itself out using the parameters you’ve given it to generate a
neural network that best can predict the output given the input.
2. Validation. Use more data to check that the training you’ve done is accurate,
or at least good enough. This should be data that you haven’t used in your
training. What we’re checking for here is that the ML model has learnt the
underlying process behind **input → output** rather than just learning that set
of data and being very good at replicating it.
3. Prediction. Your model is complete! Use new input to predict some outputs.

I am always looking at ways to segment and simplify my data so I started by
looking at colour. Do I need a colour image to predict? Not necessarily. In
fact, some transformation may help the ML do its job. To this end, I performed
edge detection analysis on my input images. This works very well for these inked
manuscripts as the difference between what is and what is not an element is
typically very sharp, and edge detection brings these sharp features into focus.

#[A sample stave, with edge detection applied](images/201001/edge-detect.png)

# 1. How many stafflines are there?

To generate a model that best answers this question, for each stave that has
already been transcribed, I tagged each stave with the number of stafflines
defined in that stave. The minimum and maximum number of stafflines across the
dataset were taken to normalise my data for training the ML model (they work
best when all numbers are between 0–1). Using this initial data, moving windows
of each stave were generated using a fixed width so that all columns of
stafflines were visible. The was to get the ML model to count the horizontal
stripes. Since all I was looking for was the stripes to be horizontal, I
generated three times more data synthetically by flipping the images upside
down, then back to front, then both.

<figure><video autoplay preload loop muted poster src="images/201001/x-window.mp4"></video><figcaption>Visualisation of a sliding window of width 20</figcaption></figure>

I found that the width of the stripe did not affect the training accuracy, so I
chose a midrange window size of 20 pixels. Each fixed–width stripe was trained
against the ML model, such that an input shape of `n,20,1 → 1` where *n* is the
height of the stripe and the output is a single number indicating a predicted
number of stafflines. The output, of course, is never a clean answer of “4”, “5”
or “6”, but lies somewhere in–between. I rounded my answer to the nearest whole
number. After training, this ML model can be used to figure out how many
stafflines to try to detect in the stave.

# 2. Which areas have stafflines in?

This model uses a much larger, square window size (60x60 pixels) and moves over
the entirety of the stave. If the end of a staffline is contained in this
window, then it is marked as being a region of interest (ROI). If no
staffline–ends are in the window, then it is marked as not interesting. I used
the image flipping method to synthetically create transformed data. Since there
is so much data created here (roughly a few thousand windows per stave), I
elected to only use the training data in one “epoch” (i.e. only let the model
look at each sample once).

<figure><video autoplay preload loop muted poster src="images/201001/xy-window.mp4"></video><figcaption>Visualisation of a sliding window of size 60</figcaption></figure>

In prediction, a window of the same size is given to the model and it predicts
whether it contains the end of a staffline, as a probability in the range 0–1.
This probability is added to a “heatmap” of probabilities of the stave to
generate a masking layer to use for the final model. Bright spots will be
processed, and dark spots ignored.

#[The ROI for the stave](images/201001/staffline-roi.png)

# 3. Where in these areas are the stafflines?

The final model runs a similar moving window, but with a smaller dimension (20
pixels). I found this window size to be optimal for only containing one
staffline–end at a time. As we will only be looking at areas marked by the
previous ROI model as containing a staffline–end, the question is not the
existence of the staffline, but where is it? This is crucial: as the ML models
are only looking at numbers, it’s tough to train them to look at existence and
location simultaneously. The model was trained using windows that had
staffline–ends in, and once again I flipped the images to generate more data.
The transformation shape was therefore from `20, 20, 1 → 2`, the output being an
`x, y` coordinate between 0–1 where `0, 0` is the top left of the window and `1,
1` the bottom right.

<figure><video autoplay preload loop muted poster src="images/201001/xy-window-image.mp4"></video><figcaption>Visualisation of a sliding window of size 20 operating over the ROI of the stave</figcaption></figure>

#[Heatmap output of the third model](images/201001/staffline-loc-heatmap.png)

In prediction, this is used once again to create bright spots on a heatmap.
Using the output from the number of lines predicted, we can make the assumption
that this number of bright spots will appear in the left half of the image, and
the same number in the right half. I therefore processed the image to find this
many bright spots in each half and matched them up: the highest spot in the left
matches with the highest spot in the right, the lowest with the lowest and so
on. We can then draw on the image where we believe the stafflines are, or use
these points as predictions for correction in the CANDR editor. This output was
created with only a few staves’ worth of data input, so naturally the prediction
is going to improve as more staves are input, giving these ML models more
training data. It is likely that I will be creating and refining more ML models
to detect other features, such as divisiones, clefs, accidentals, text syllables
and notes. I can foresee that these ML predictions will not serve as perfect
transcriptions of the images, but quick, mostly–there transcriptions that will
only require a little tweaking on my part.

#[Processed output from the final model](images/201001/stafflines-drawn.png)

[^1]: Joshua Stutter, ‘CANDR’, *GitLab*, <[https://gitlab.com/yockyrr/candr](https://gitlab.com/yockyrr/candr)>
[accessed 1 October 2020]; Joshua Stutter, ‘CANDR Data’, *GitLab*,
<[https://gitlab.com/yockyrr/candr-data](https://gitlab.com/yockyrr/candr-data)>
[accessed 1 October 2020]

[^2]: Joshua Stutter, ‘CANDR Data /imagestore’, *GitLab* <[https://gitlab.com/yockyrr/candr-data/-/tree/master/imagestore](https://gitlab.com/yockyrr/candr-data/-/tree/master/imagestore)>
[accessed 1 October 2020]

[^3]: Joshua Stutter, ‘CANDR /scripts/foliator.sh’, *GitLab* <[https://gitlab.com/yockyrr/candr/-/blob/master/scripts/foliator.sh](https://gitlab.com/yockyrr/candr/-/blob/master/scripts/foliator.sh)>
[accessed 1 October 2020]; Joshua Stutter, ‘CANDR /scripts/uploader.sh’,
*GitLab* <[https://gitlab.com/yockyrr/candr/-/blob/master/scripts/uploader.sh](https://gitlab.com/yockyrr/candr/-/blob/master/scripts/uploader.sh)>
[accessed 1 October 2020]

[^4]: Joshua Stutter, ‘Uploaded W1 (c74fac5a)’, *GitLab* <[https://gitlab.com/yockyrr/candr-data/-/commit/c74fac5ac9a586da33ca84db7b809db569c17bcf](https://gitlab.com/yockyrr/candr-data/-/commit/c74fac5ac9a586da33ca84db7b809db569c17bcf)>
[accessed 1 October 2020]

[^5]: Confusingly, OMR more famously stands for optical *mark* recognition, the
kind used for Scantron™ multiple choice tests, so a blind search for “OMR”
won’t necessarily yield much music!

[^6]: ‘Optical Music Recognition (OMR)’, *DDMAL* <[https://ddmal.music.mcgill.ca/research/omr/](https://ddmal.music.mcgill.ca/research/omr/)>
[accessed 1 October 2020]

[^7]: *The Gamera Project* <[https://gamera.informatik.hsnr.de/](https://gamera.informatik.hsnr.de/)>
[accessed 1 October 2020]

[^8]: ‘Gamera Addon: MusicStaves Tookit’, *The Gamera Project* <[https://gamera.informatik.hsnr.de/addons/musicstaves/](https://gamera.informatik.hsnr.de/addons/musicstaves/)>
[accessed 1 October 2020]

[^9]: ‘DDMAL/Pixel.js Wiki’, *GitHub* <[https://github.com/DDMAL/Pixel.js/wiki](https://github.com/DDMAL/Pixel.js/wiki)>
[accessed 1 October 2020]

[^10]: ‘Pixel.js: Web–based Pixel Classification Correction Platform for Ground
Truth Creation’, *SIMSSA* <[https://simssa.ca/assets/files/GREC2017-Ke-Zeyad.pdf](https://simssa.ca/assets/files/GREC2017-Ke-Zeyad.pdf)>
[accessed 1 October 2020]

[^11]: *TensorFlow* <[https://www.tensorflow.org/](https://www.tensorflow.org/)>
[accessed 1 October 2020]

[^12]: ‘Keras: the Python deep learning API’, *Keras* <[https://keras.io/](https://keras.io/)>
[accessed 1 October 2020]
