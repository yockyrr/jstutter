module.exports = function(eleventyConfig) {
	/* Pass through - stop eleventy touching */
	eleventyConfig.addPassthroughCopy('src/images')
	/* Stolen from https://www.11ty.dev/docs/languages/markdown/ */
	const markdownIt = require("markdown-it");
	const markdownItFootnote = require("markdown-it-footnote");
	const markdownItFigure = require("markdown-it-figure");
	const markdownItMermaid = require("markdown-it-mermaid").default;
	const markdownItEmoji = require("markdown-it-emoji");
	const cryptoStr = require('crypto-random-string');
	const htmlmin = require("html-minifier");

	const options = {
		html: true,
		typographer: true
	};
	const markdownLib = markdownIt(options)
		.use(markdownItFootnote)
		.use(markdownItFigure)
		.use(markdownItMermaid)
		.use(markdownItEmoji);
	markdownLib.renderer.rules.footnote_anchor_name = function(tokens, idx, options, env) {
		var n = Number(tokens[idx].meta.id + 1).toString();
		if(!env.docId) {
			env.docId = cryptoStr({length: 10, type: 'url-safe'});
		}
		var prefix = '';
		if(typeof env.docId === 'string') {
			prefix = '-' + env.docId + '-';
		}
		return prefix + n;
	}
	eleventyConfig.setLibrary("md", markdownLib);
	eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
		if(typeof outputPath === 'string' && outputPath.endsWith(".html")) {
			let minified = htmlmin.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: false
			});
			return minified;
		}
		return content;
	});
	return {
		dir: { input: 'src', output: 'dist', data: '_data' },
		passthroughFileCopy: true,
		templateFormats: ['njk', 'md', 'css', 'html', 'yml'],
		htmlTemplateEngine: 'njk'
	};
}
